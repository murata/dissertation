As motivated in the previous chapter, the uncertainty of vibro-measurements holds a significant role in the success of vibro-localization techniques.
Prior work has attempted various methods to quantify and remedy the effects of errors contained in vibro-measurement vectors.
There exist approaches attempting to remedy these errors in the vibro-localization context by employing Kalman filtering framework ~\cite{alajlouni2020passive}, \gls{mle}~\cite{baker2022maximum}, or sensor elimination methods~\cite{mirshekari2018occupant} to minimize as small information as possible.
While these frameworks multiple sensors to be employed to remedy the effects of error in vibro-measurement vectors, there is limited effort in the quantification of the vibro-localization errors when a single sensor is employed.
% However, there is a gap in the literature in identifying the effect measurement errors to the vibro-localization errors. 

\section{Introduction}\label{sec:ch2-introduction}
In this chapter, we study the localization outcomes of an energy-based vibro-localization technique in which sensor disturbances of a single sensor such as sensor bias and noise govern the uncertainty of the location estimations.
In order to study these errors, we employ different statistical methods to quantify the bounds of the mean and \gls{pdf} of the vibro-localization errors. 

Portions of the content covered in this chapter have been previously presented in \cite{ambarkutuk2022}.
This work has laid the groundwork for further research, particularly in the context of exploring the uncertainties inherent in localization estimations using accelerometer data.
Building upon these foundational insights, the current dissertation extends the scope by delving deeper into the uncertainty of energy-based vibro-localization techniques, especially when dealing with a multi-sensor setup.
The methodologies and findings from \cite{ambarkutuk2022} have been instrumental in shaping the research approach and analysis presented in this chapter.

\subsection{Summary of the Contributions}
\label{sec:ch2-summary-contributions}
This chapter makes several key contributions to the field of vibro-localization, particularly in the context of single-sensor setups:

\begin{itemize}    
    \item \textbf{Comprehensive Methodological Framework}: Development and implementation of a rigorous statistical framework to quantify and analyze the errors in vibro-localization. This includes a novel approach to model sensor measurement imperfections and their impact on localization accuracy.

    \item \textbf{Analytical and Numerical Analysis of Localization Error}: The chapter employs both theoretical and numerical methods to estimate the mean localization error, considering factors like sensor noise and signal-to-noise ratio (SNR).
    
    \item \textbf{Practical Insights for Vibro-Localization Systems}: The findings offer practical insights for the design and improvement of vibro-localization systems, highlighting the critical role of sensor noise and SNR in achieving accurate localization.
\end{itemize}

These contributions are expected to provide a foundation for future research in the field, particularly in enhancing the accuracy and reliability of vibro-localization systems in practical applications.

\subsection{Outline}
\label{sec:ch2-outline}
This chapter focuses on the uncertainties in single-sensor energy-based vibro-localization techniques.
The structure of the chapter is as follows:

\begin{enumerate}
    \item \Cref{sec:ch2-introduction} introduces the context and significance of vibro-measurement uncertainties in vibro-localization, reviewing prior work and establishing the main focus of the chapter.
    \item \Cref{sec:ch2-problem} lays the foundational definitions for the study, including localization space, sensor measurements, and the concept of vibro-measurement vector energy.
    \item \Cref{sec:ch2-method} presents the methodology for quantifying and analyzing errors in single-sensor vibro-localization, including the development of the Parametric Energy Decay Model and the derivation of the PDF of localization error.
    \item \Cref{sec:ch2-parametric} conducts a parametric study to examine the impact of sensor noise and SNR on localization accuracy, employing theoretical and empirical analyses.
    \item \Cref{sec:ch2-conclusions} summarizes the key findings, emphasizing the influence of sensor noise and SNR on vibro-localization accuracy and the effectiveness of the applied analytical methods.
\end{enumerate}


\section{Problem Definition}\label{sec:ch2-problem}
In this study, we consider a single sensor energy-based vibro-localization technique to localize an occupant in an indoor environment.
This localization technique can be considered as an elemental building-block for much larger vibro-localization techniques.
Therefore, understanding the error characteristics of such localization techniques will reveal how to improve complex vibro-localization techniques.

To formally state the localization problem in hand, we put forward a set of definitions such the localization space, sensor's measurement ability, etc.
% We start by defining the localization space \gls{setS}.
The localization space \gls{setS} is a bounded area in a building which contains both the occupant and sensor.
\begin{definition}[Occupant and Localization Space \gls{setS}]
    Localization space \gls{setS} is a closed subset of a building which contains a sensor tracking a dynamic occupant.
    In other words, the localization space \gls{setS} is a set of points that an occupant can reside, and is defined with respect to the sensor.

    An occupant within the context of this study refers to a dynamic and non-evasive target residing in the localization space \gls{setS}. The dynamic nature of the occupant implies movement or potential for movement within the confines of \gls{setS}, and the term ``non-evasive'' denotes that the occupant's behavior is not intentionally altering to avoid detection or localization. The occupant's presence and movement are key factors influencing the sensor measurements, and thus, they are critical to the accuracy and effectiveness of the vibro-localization technique.
    \label{dft:localization-space}
\end{definition}

With the definition of localization space \gls{setS}, we can define an event with which the occupant is localized.

\begin{definition}[Event: Footstep]
    An event, specifically a footstep, is characterized as a distinct occurrence within the localization space \gls{setS} that can be identified by sensor measurements.
    The localization events are phenomena that results some changes in the localization space \gls{setS}.
    Within a set of time-steps \gls{setK} $= \{1, \ldots, \text{\gls{nk}}\}$, a footstep event is represented by a unique pattern in the sensor data that corresponds to the physical act of an occupant taking a step.
    \label{dft:event-footstep}
\end{definition}

The measured data often exhibit a sharp, impact-like waveform, akin to a blip on a \gls{sonar} screen.
This waveform is characterized by a sudden onset that overcomes the ambient noise, followed by a rapid decay that diminishes quickly below the surrounding ambient noise.
This pattern is discernible from other ambient sensor readings due to its distinctive signal characteristics, which include amplitude, duration, and frequency.
The identification and analysis of these footsteps are crucial for the precise localization of the occupant within \gls{setS}.
Considering each event results in unique perturbations and a set of sensors observing them in the ambient space, our task is to distinguish \gls{toi} from the background and provide an estimated location vector \gls{xhat} corresponding to it.

In this study, the measurements of a single accelerometer are used to localize the occupant.
In order to derive the localization problem, we put forward a model for the sensor measurements as given in \Cref{dft:sensor-measurement}.

\begin{definition}[Sensor Measurement]
    Let $\hat{z}[k] \in \mathbb{R}$ be the time-domain measurement the sensor obtained at time step $k$, then we can define measurement vector $\hat{\vect{z}}$ can be defined as:
    \begin{equation}
        \hat{\vect{z}} = \hat{z}\left[\forall k \in \mathcal{K}\right] = (\hat{z}[1], \ldots, \hat{z}[n_k])^\top \in \mathbb{R}^{n_k}~.
    \end{equation}

    Without loss of generality, we model sensor imperfections $\vect{\zeta}$ as
    the sum of a constant sensor bias $\delta \in \mathbb{R}$ and random measurement error vector $\vect{\nu} = (\nu[1], \ldots, \nu[n_k])^\top \in \mathbb{R}^{n_k}$.
    Hence, the full signal model can be represented as shown in \Cref{eq:signal}.
    \begin{equation}
        \label{eq:signal}
        \hat{\vect{z}} = \vect{z} + \vect{\zeta} = \vect{z} + \vect{\nu} + \delta~.
    \end{equation}
    % where the random measurement error vector is given by $\vect{\nu} = (\nu_1, \ldots, \nu_{n_k})^\top \in \mathbb{R}^{n_k}$.
    \label{dft:sensor-measurement}
\end{definition}

Given these definitions, we can define the signal energy with which the occupant localization is derived.
In this study, we employ the energy of the measured vibro-measurement vector as the localization feature.
The formal definition of the localization feature is given in~\Cref{dft:localization-feature}.

\begin{definition}[Energy of Vibro-measurement Vector]
    Energy of a random measurement vector can be obtained by employing Rayleigh's Energy Theorem, given in~\Cref{thm:rayleigh-deterministic}, on the random vibro-measurement vectors $\vect{\hat{z}}$.
    The energy of a vibro-measurement vector is formally given below,
    \begin{equation}
        \hat{e} = \norm{\hat{\vect{z}}}_2^2 = \hat{\vect{z}}^\top \hat{\vect{z}} = \sum_{k=1}^{n_k} \hat{z}[k]^2~.
    \end{equation}
    Because we model vibro-measurements as random vectors, the energy representation of the measurement vector is also random as shown below.

    \begin{equation}
        \hat{e} = e + \varepsilon~,
    \end{equation}
    where $e = \norm{\vect{z}}_2^2$ and $\varepsilon$ represent the energy of the true signal and error in energy representation.
    \label{dft:localization-feature}
\end{definition}

By using the definition of the energy of vibro-measurement vectors, we can now formally define a localization function that determines the occupant location $\vect{\hat{x}}$ from the energy $e$ of the measurement vector $\vect{z}$.

\begin{definition}[Energy-based Vibro-localization Function $\vect{h}(e; \cdot)$]
    Without loss of generality, we provide an abstract definition of a localization function that maps the energy of an vibro-measurement vector to a location vector.
    The equation below represent this localization function in terms of the distance $d$ and the directionality $\theta$ defined between the sensor and the occupant.
    In other words, when the true energy $e$ is known, the localization function $\vect{h}(e; \cdot)$ should yield the true location $\vect{x}$.
    \begin{equation}
        \label{eq:localization-function}
        \vect{x} = \vect{h}(e; \cdot) = d \begin{bmatrix}
            \cos{\theta} \\
            \sin{\theta}
        \end{bmatrix}~.
    \end{equation}

    By using the same representation, we can derive the estimated location vector $\vect{\hat{x}}$ from the energy $\hat{e}$ of a noisy and bias drifted vibro-measurement $\vect{\hat{z}}$ vector as shown below,
    \begin{equation}
        \label{eq:localization-function-noisy}
        \hat{\vect{x}} = \vect{h}(\hat{e}; \cdot) = \hat{d} \begin{bmatrix}
            \cos{\hat{\theta}} \\
            \sin{\hat{\theta}}
        \end{bmatrix}~.
    \end{equation}
    \label{dft:single-localization-function}
\end{definition}

Given the definition of the estimated and true location vectors, i.e., $\vect{\hat{x}}$ and $\vect{x}$, we can now define a vector representing the localization error.
A successful vibro-localization technique should minimize all statistical moments of this error vector when repeated measurements are done.

\begin{definition}[Localization Error]
    The localization error $\vect{\chi} \in \mathbb{R}^2$ can be defined as:
    \begin{equation}
        \label{eq:localization_error}
        \vect{\chi} = \vect{x} - \vect{\hat{x}}~.
    \end{equation}
    Hence, the magnitude of the estimation error can then be represented as the norm of the difference between the estimated and the true location as,
    \begin{equation}
        \label{eq:location_error}
        \norm{\vect{\chi}} = \norm{\vect{x} - \vect{\hat{x}}}~.
    \end{equation}
\end{definition}

\Cref{fig:single-sensor-layout} provides a graphical representation of the defined terminology.
\input{figs/single-sensor.tikz}

\begin{definition}[Accuracy and Precision]
    The statistical properties of localization error $\vect{\chi}$ define the \emph{accuracy} and \emph{precision} of a vibro-localization system.
    Accuracy is a measure of how close the estimated localizations are to the true values, typically represented by the mean of the error vector, $\vect{\mu}_{\vect{\chi}}$.
    A smaller mean error indicates higher accuracy, as it implies that on average, the estimations are closer to the true location.
    Precision, on the other hand, refers to the consistency of the estimations, often quantified by the covariance matrix $\vect{\Sigma}_{\vect{\chi}}$.
    The covariance provides a measure of the spread of the localization errors around the mean.
    A smaller covariance implies higher precision, indicating that the estimations are more consistently clustered around the mean, regardless of whether that mean is close to the true value.
    Thus, $\vect{\mu}_{\vect{\chi}}$ and $\vect{\Sigma}_{\vect{\chi}}$ are directly related to the accuracy and precision of the localization system, respectively. 
\end{definition}
\section{Method}\label{sec:ch2-method}

In this chapter, we delve into the methodological aspects of quantifying and analyzing the errors inherent in single-sensor energy-based vibro-localization.
Our approach is grounded in a rigorous statistical framework, aimed at comprehensively understanding and mitigating the uncertainties that impact vibro-localization accuracy.
We focus on the energy of the vibro-measurement vector and its error characteristics, recognizing that these form the crux of the localization process.
Through this methodology, we aim to strengthen the reliability and precision of vibro-localization techniques in practical applications.

In this work, we operate under a set of assumptions to streamline our analysis.
Firstly, we consider the error in directionality to be negligible, with the difference $\lvert \theta - \hat{\theta}\rvert$ approximating zero.
Regarding the measurement errors, we model the random measurement error $\nu[k]$ as independent and identically distributed (iid) realizations of a normally distributed random variable $\nu \sim \N{0}{\sigma_\zeta}$ with zero mean and standard deviation $\sigma_\zeta \in \mathbb{R}^+$.
Additionally, we ensure that the number of samples $n_k$ is sufficiently large, specifically $n_k \geq 20$, to ensure reliable statistical analysis.
For the sake of brevity, we assume in this chapter that the energy $e$ of the true signal $\vect{z}$ is known.

% \begin{itemize}
%     \item Error in directionality is neglible $\lvert \theta - \hat{\theta}\rvert \approx 0$.
%     \item Sensor bias is zero $\delta = 0$.
%     \item Random measurement error $\nu[k] \sim \N{0}{\sigma_\zeta}$ and \emph{iid}.
%     \item $n_k$ is large enough ($n_k \geq 20$)
% \end{itemize}

% In this work, we assume that the random measurement errors $\nu[k]$ are indepdent and identically distributed realizations of a normally distributed random variable $\nu \sim \N{0}{\sigma_\zeta}$ with zero mean and standard deviation $\sigma_\zeta \in \mathbb{R}^+$.
\begin{lemma}
    If random measurement error $\nu[k] \sim \N{0}{\sigma_\zeta}$ and \emph{iid}, then signal energy is \emph{approximately} normally distributed.
\end{lemma}

\begin{proof}
    In light of the assumptions above, the error in the calculated energy can be stated as,
    \begin{subequations}
        \label{eq:energy_error}
        \begin{align}
            \varepsilon &= \hat{e} - e = \sum\limits_{k=1}^n \hat{z}[k]^2 - \sum\limits_{k=1}^n z[k]^2 \\
            &= \sum\limits_{k=1}^n \hat{z}[k]^2 - z[k]^2 = \sum\limits_{k=1}^n \left(z[k] + \nu[k] + \delta \right)^2 - z[k]^2 \sim \N{\mu_{\varepsilon}}{\sigma_{\varepsilon}}
        \end{align}
    \end{subequations}
    where $\mu_\varepsilon = n \left(  \delta^2 + \sigma_\nu^2 \right) + 2 \delta \sum z[k]$, $\sigma_\varepsilon=2 n \sigma_\nu^2 \left(2 \delta^2 + \sigma_\nu^2 + 2 e \right)$ and $\N{\mu}{\sigma}$ denotes the \gls{pdf} of the Normal distribution with the mean $\mu$ and standard deviation $\sigma$.
    Therefore, the density of the error in the signal energy can be derived as shown below,
    \begin{equation}
        \boxed{
            \varepsilon \sim \f{\varepsilon} = \N{\mu_\varepsilon}{\sigma_\varepsilon} = \frac{1}{ \sigma_\varepsilon \sqrt{2 \pi} } \exp{\left(-\frac{1}{2} \left(\frac{\varepsilon - \mu_\varepsilon}{\sigma_\varepsilon}\right)^2 \right)}~.
        }
    \end{equation}

    In light of this derivation, the expected error in the calculated energy is a function of the number of samples in the signal $n$, bias $\delta$ and the standard deviation of the random errors $\sigma_\zeta$.
\end{proof}

A pivotal aspect of our methodology involves the Parametric Energy Decay Model, a crucial tool in understanding the dynamics of energy decay in vibro-localization.
This model encapsulates the relationship between the energy of vibro-measurements and the corresponding spatial parameters.
Specifically, it characterizes how the energy of a vibro-measurement vector decays with respect to distance, thereby providing a fundamental link between the measurable energy and the occupant's location.
The model is defined mathematically as follows:

\begin{definition}[Parametric Energy Decay Model]
    The Parametric Energy Decay Model is an analytical tool used to describe the relationship between the energy of vibro-measurement vectors and the distance to the source of the vibration.
    This model is crucial for understanding how energy dissipates over space, which directly impacts the localization accuracy in vibro-localization systems.
    The model is defined with two key parameters, $\beta$ and $e^*$, where $\beta$ (a negative real number) represents the rate of energy decay and $e^*$ (a positive real number) signifies a reference energy level.
    The distance $d$ to the vibration source is a function of the measured energy $e$, given by:
    \begin{equation}
        d = g(e; \beta, e^*) = \frac{1}{\beta} \log{\frac{e}{e^*}}~.
    \end{equation}
    Similarly, the estimated distance $\hat{d}$ is derived from the estimated energy $\hat{e}$, following the same functional form:
    \begin{equation}
        \hat{d} = g(\hat{e}; \beta, e^*) = \frac{1}{\beta} \log{\frac{\hat{e}}{e^*}}~.
    \end{equation}
    The role of $\theta$, the direction angle of the occupant relative to the accelerometer, is also critical in the context of localizing the source.
    By applying these formulations to the established localization function (\Cref{eq:localization-function}), we can quantify the magnitude of the localization error in terms of energy measurement errors.
    This is represented as the norm of the vector $\vect{\chi}$, which quantifies the difference between the estimated and actual location:
    \begin{equation}
        \boxed{
            \norm{\vect{\chi}} = \Delta_{\varepsilon}\left( \varepsilon; \beta \right) = -\frac{1}{\beta}\log{\left(\frac{\hat{e}}{e}\right)} = -\frac{1}{\beta}\log{\left(\frac{e + \varepsilon}{e}\right)} = -\frac{1}{\beta}\log{\left(1 + \frac{\varepsilon}{e}\right)}~.
        }
    \end{equation}
\end{definition}

\subsection{\gls{pdf} of the Localization Error}
\gls{pdf} transformation theorem allows us to derive the \gls{pdf} of a function of random variable when the function is invertible.
Formal definition of this theorem is given in~\Cref{thm:density}.

By employing normed localization error $\norm{\vect{\chi}}$ from the \gls{pdf} of the calculated energy errors, we have:

\begin{subequations}
    \begin{align}
        \f{\norm{\vect{\chi}}} &= f_{\varepsilon} \left( \Delta_{\varepsilon}^{-1}(\norm{\vect{\chi}})\right) \left\lvert \frac{\partial}{\partial \norm{\vect{\chi}}} \Delta_{\varepsilon}^{-1}\left(\norm{\vect{\chi}}\right)\right\rvert \\
        &= - \beta e \exp{\left(\beta \norm{\vect{\chi}}\right)} f_{\varepsilon} \left( e \exp{\left(\beta \norm{\vect{\chi}}\right)} - e \right)
    \end{align}
\end{subequations}
where $\Delta_{\varepsilon}^{-1}(\norm{\vect{\chi}}) = e \exp{\left(\beta \norm{\vect{\chi}} \right)} - e$ and $\lvert\frac{\partial}{\partial \norm{\vect{\chi}}} \Delta_{\varepsilon}^{-1}\left(\cdot\right)\rvert = -\beta e \exp{\left(-\beta \norm{\vect{\chi}}\right)}$.
Finally putting everything together, we derive \gls{pdf} of localization error:
\begin{equation}
    \boxed{
        \Rightarrow \f{\norm{\vect{\chi}}} = - \frac{\beta e \exp{(\beta \norm{\vect{\chi}})}}{\sigma_{\varepsilon} \sqrt{2\pi}} \exp\left(-\frac{\left(e \exp{(\beta \norm{\vect{\chi}})} - e - \mu_{\varepsilon}\right)^2}{2\sigma_{\varepsilon}^2}\right)~.
    }
    \label{eq:pdf-localization-error}
\end{equation}

\begin{remark}
    \Cref{eq:pdf-localization-error} implies that $\f{\norm{\vect{\chi}}}$ follows a normal distribution iff the error function $\Delta_{\varepsilon}$ is linear.
\end{remark}

With the calculated \gls{pdf} derived in \Cref{eq:pdf-localization-error}, we can derive the mean and the variance of the normed error term as:

\begin{equation}
    \boxed{
        \mu_{\norm{\vect{\chi}}} = \expt{\norm{\vect{\chi}}} =  \int_{0}^{\infty} \norm{\vect{\chi}} \, \f{\norm{\vect{\chi}}} \,\diff \norm{\vect{\chi}}
    }
\end{equation}
and
\begin{equation}
    \boxed{
        \sigma_{\norm{\vect{\chi}}}^2 = \Var{\norm{\vect{\chi}}} = \expt{\norm{\vect{\chi}}^2} =  \int_{0}^{\infty} \norm{\vect{\chi}}^2 \, \f{\norm{\vect{\chi}}} \,\diff \norm{\vect{\chi}}
    }
\end{equation}

As can be seen in the equations above, the closed form solution for the mean and variance is computationally challenging due to the complexity of the integrals involved.
In order to tackle this problem, we employ an numerical method that enables us to calculate the moments of the normed localization error from the \gls{pdf} of the energy error $\f{\varepsilon}$ and error function $\Delta_{\varepsilon}(\varepsilon)$.

\subsection{First Statistical Moment of Localization Error}
In order to derive the first moment, i.e., the mean, of the localization error, \gls{lotus} is employed.
\gls{lotus} is a theorem used to calculate the expectation of a  function of random variable with its own \gls{pdf}.
Hence, we can state the mean as given by,
\begin{equation}
    \label{eq:mean}
    \mu_{\norm{\vect{\chi}}} = \expt{\Delta_{\varepsilon}(\varepsilon)} = \int \Delta_{\varepsilon}(\varepsilon) \, \f{\varepsilon} \,\diff{\varepsilon}~.
\end{equation}

Furthermore, we employ Taylor expansion to the error function $\Delta_{\varepsilon}(\varepsilon)$ around $\frac{\mu_\varepsilon}{e}$ to analytically obtain the value of the integral given in \Cref{eq:mean}.
\begin{equation}
    \Delta_{\varepsilon}(\varepsilon) \approx \hat{\Delta}_{\varepsilon}(\varepsilon) = \frac{1}{\beta} \left(- \log{\left(\frac{\mu_\varepsilon}{e} + 1 \right)} + \sum_{k=1}^{\infty} \frac{\left(-1\right)^{k}}{k \left(e + \mu_{\varepsilon} \right)^k} \left(\varepsilon - \mu_\varepsilon \right)^k \right)
\end{equation}

This analytical approach is as given by,
\begin{align*}
    \mu_{\norm{\vect{\chi}}} &\approx \hat{\mu}_{\norm{\vect{\chi}}} = \int \hat{\Delta}_{\varepsilon}(\varepsilon) \, f(\varepsilon) \,d\varepsilon~,
    \intertext{where $\hat{\Delta}_{\varepsilon}(\varepsilon)$ is approximated using its Taylor series expansion around $\mu_\varepsilon$. Expanding the error function $\hat{\Delta}_\varepsilon(\varepsilon)$, we obtain,}
    &\approx \int -\frac{1}{\beta} \left( \log{\left(\frac{\mu_\varepsilon}{e} + 1\right)} - \sum_{k=1}^{\infty} \frac{\left(-1\right)^{k}}{k \left(1 + \mu_\varepsilon\right)^k} \left(\varepsilon - \mu_\varepsilon \right)^k\right) \, f(\varepsilon) \,d\varepsilon~.
    \intertext{Further we split the integral into two parts: one involving the $0^{th}$ approximation and the other involving the higher orders,}
    &= -\frac{1}{\beta} \int  \log{\left(\frac{\mu_\varepsilon}{e} + 1\right)}\, f(\varepsilon) \,d\varepsilon + \frac{1}{\beta} \int \sum_{k=1}^{\infty} \frac{\left(-1\right)^{k}}{k \left(e + \mu_\varepsilon\right)^k} \left(\varepsilon - \mu_\varepsilon \right)^k \, f(\varepsilon) \,d\varepsilon~.
    \intertext{The first integral evaluates to the logarithmic term times the integral of the probability density function over all space, and the second integral is expanded term-by-term,}
    &= -\frac{1}{\beta} \log{\left(\frac{\mu_\varepsilon}{e} + 1\right)} \int f(\varepsilon) \,d\varepsilon + \frac{1}{\beta} \sum_{k=1}^{\infty} \int \frac{\left(-1\right)^{k}}{k \left(e + \mu_\varepsilon\right)^k} \left(\varepsilon - \mu_\varepsilon \right)^k \, f(\varepsilon) \,d\varepsilon~.
    \intertext{Each term in the series is integrated separately, and these integrals correspond to the central moments of the distribution of $\varepsilon$.}
    &= -\frac{1}{\beta} \log{\left(\frac{\mu_\varepsilon}{e} + 1\right)} + \frac{1}{\beta} \sum_{k=1}^{\infty} \int \frac{\left(-1\right)^{k}}{k \left(e + \mu_\varepsilon\right)^k} \left(\varepsilon - \mu_\varepsilon\right)^k \, f(\varepsilon) \,d\varepsilon
    \intertext{The series is expressed in terms of the central moments, denoted as $\alpha_k$.}
    &= -\frac{1}{\beta} \log{\left(\frac{\mu_\varepsilon}{e} + 1\right)} + \frac{1}{\beta} \sum_{k=1}^{\infty} \frac{\left(-1\right)^{k}}{k \left(e + \mu_\varepsilon\right)^k} \int \left(\varepsilon - \mu_\varepsilon\right)^k \, f(\varepsilon) \,d\varepsilon
    \intertext{Finally, using the definition of $\alpha_k$ for even and odd terms, the expression is simplified to its final form.}
    &= -\frac{1}{\beta} \log{\left(\frac{\mu_\varepsilon}{e} + 1\right)} + \frac{1}{\beta} \sum_{k=1}^{\infty} \frac{\left(-1\right)^{k} \alpha_k}{k \left(e + \mu_\varepsilon\right)^k}~,
\end{align*}
where $\alpha_{2k} = (2k-1)!! \sigma_\varepsilon^{2k}$ and $\alpha_{2k+1} = 0$; therefore,
\begin{equation}
    \boxed{
        \mu_{\norm{\vect{\chi}}} \approx \hat{\mu}_{\norm{\vect{\chi}}} = -\frac{1}{\beta} \log{\left(\frac{\mu_\varepsilon}{e} + 1\right)} + \frac{1}{2\beta} \sum_{k=1}^{\infty} \frac{\left(-1\right)^{2k} (2k-1)!! \sigma_\varepsilon^{2k}}{k \left(e + \mu_\varepsilon\right)^{2k} \, e^{2k}}~.
    }
    \label{eq:lotus-mean}
\end{equation}

\begin{remark}
    As can be seen in~\Cref{eq:lotus-mean}, the mean vibro-localization error is zero, iff $\mu_\varepsilon = \sigma_\varepsilon = 0$.
\end{remark}

\section{Parametric Study and Results}\label{sec:ch2-parametric}
This section presents a parametric study of the single-sensor energy-based vibro-localization technique.
The study focuses on the impact of sensor noise and \gls{snr} on the localization accuracy.
The methodological approach combines theoretical and empirical analyses, primarily using a Taylor series approximation and numerical integration.

The localization error was analytically estimated by expanding the error function \(\Delta_\varepsilon(\varepsilon)\) using a Taylor series. This approach allowed for an approximate calculation of the mean localization error across different orders of approximation. The study varied key parameters like the \gls{snr} and the standard deviation of the sensor noise \(\sigma_\varepsilon\) to observe their effects on the localization accuracy.

The results, illustrated in~\Cref{fig:ambarkutuk-mean-estimation}, show the percent error in mean localization estimation across different \gls{snr} values (-5 dB, 0 dB, and 5 dB) and varying \(\sigma_\varepsilon\) (1, 3, and 5). The Taylor series approximation was considered up to the 4th order. The findings indicate that:

\begin{itemize}
    \item Higher \gls{snr} values generally result in lower percent errors, highlighting the significance of \gls{snr} in vibro-localization accuracy.
    \item Increasing \(\sigma_\varepsilon\) leads to higher percent errors, demonstrating the sensitivity of the localization process to sensor noise.
    \item The accuracy of the Taylor series approximation improves with higher orders, as evidenced by the decrease in percent error.
\end{itemize}

The parametric study underscores the importance of considering sensor noise characteristics and \gls{snr} in designing and deploying vibro-localization systems.
The Taylor series approximation provides a useful tool for estimating the mean localization error, especially in scenarios where exact analytical solutions are challenging to derive.
Even with smaller approximation order ($k \leq 5)$, the proposed numeracial approach was able to identify the localization error.

\begin{figure}[!htb]
    \centering
    \includegraphics[width=\textwidth]{figs/ambarkutuk_mean_estimation-1.eps}
    \caption[Analytical Evaluation of Mean Localization Error]{This figure illustrates the percent error in the mean localization estimation across varying signal-to-noise ratio (\gls{snr}) levels of -5 dB, 0 dB, and 5 dB, and different standard deviations of sensor noise \(\sigma_\varepsilon\) values of 1, 3, and 5. The analysis is conducted using a Taylor series approximation up to the 4th order. The results depict the sensitivity of vibro-localization accuracy to both \gls{snr} and sensor noise, highlighting the reduction in percent error with increasing \gls{snr} and decreasing sensor noise. The improvement in approximation accuracy with higher-order Taylor series expansions is also evident.}
    \label{fig:ambarkutuk-mean-estimation}
\end{figure}

\section{Conclusions}\label{sec:ch2-conclusions}
This chapter explored the uncertainties in single-sensor energy-based vibro-localization techniques.
The study revealed:

\begin{itemize}
    \item Both sensor noise and \gls{snr} significantly affect the accuracy of vibro-localization.
    Higher \gls{snr} and lower sensor noise improve localization precision.
    \item The use of a Taylor series approximation for the mean localization error is effective, especially for higher-order expansions.
    \item Localization errors seldom follow a normal distribution.
    \item The mean localization error is never zero if the vibro-measurement vectors contain inherent errors.
\end{itemize}

The following chapter explores the multi-sensor setups in which a sensor fusion technique was employed to combine the localization results of each sensor to compensate for the limitations identified in single-sensor systems with empirical studies.
