Target localization refers to determining the position of a specific entity or an object, or commonly known as \gls{toi} in a fixed space.
In the context of \gls{iol}, the \glspl{toi} are the occupants and the space is the indoor environment, such as a building or room.
By employing an \gls{iol} technology, an interface between the occupants and the building is established~\cite{yan2017iea,becerik2022ten}.
This interface can be used in numerous applications, such as energy management by adjusting heating or cooling based on occupancy~\cite{howard2013forecasting,turley2020development,chen2017occupancy}, ensuring safety during emergency evacuations, monitoring patient movements in healthcare settings, or enhancing personalized experiences in smart homes.

However, there are also significant concerns related to \gls{iol} such as privacy, cost and accuracy to name a few.
The primary among these is the issue of privacy.
As occupants are continuously tracked, there is a risk of misuse of this data or unauthorized malicious access, potentially revealing sensitive information about an individual's habits, routines, or health conditions.
Additionally, there may be discomfort among users in being constantly monitored, leading to feelings of surveillance.

The choice of sensing methodology employed in an indoor localization also plays a pivotal role in the prospect of the localization outcomes.
Visual sensing modality, for example, have gained popularity in the realm of \gls{iol} due to their accuracy and ability to provide rich contextual information.
However, these systems often come with an exceptionally high cost.
The main reason for this drawback is that the cameras are limited by their \gls{fov}; thus, many of them need to be deployed to cover the localization space.
% Furthermore, the required high-resolution cameras, along with the necessary computational infrastructure for image processing and analysis, can make the technology prohibitively expensive.
This not only increases the initial investment for setting up such systems but can also inflate the ongoing maintenance and upgrade costs.
% As a result, the high expenses associated with visual sensing modality limit the accessibility of this technique, potentially making it an exclusive option for only those institutions or individuals with significant resources.
% Such economic barriers further complicate the equitable distribution and application of \gls{iol} technologies.

\Cref{fig:spider_existing} illustrates a comparison between different sensing modalities frequently employed in occupant localization systems.
Overall, such sensor modalities fall under two sensing schemes: active and passive.
The latter relies on the measurements of the ambient space to deduce the occupant location by tracking the changes in the localization space.
Therefore, these techniques do not rely on special beacons, known features, etc to track across the localization space.
For instance, visual, acoustic and vibration sensing are generally examples of passive schemes.
On the other hand, the active schemes require occupants to carry a special feature that can be recognized by the localization system.
In other words, the detection and localization of the occupant often conducted by tracking such known features in the space.
Therefore, active perception schemes tend to yield more accurate and precise estimations about the occupant's location.
\gls{mocap} is an example of such localization systems where the occupants carry special reflecting markers and the localization space is illuminated with special sources that emit light modulated at a specific frequency.
The occupants are localized by tracking the reflections with multiple cameras tuned to detect only the modulated light.
The use of such knowledge manifets itself as increased localization accuracy and precision.

\input{figs/spider.tikz}

This document provides a deep look into existing \gls{iol} techniques and proposes a new approach that does not require expensive sensors and infringe upon occupants' privacies.
Aiming to make \gls{iol} both economically feasible and ethically sound, our approach seeks to democratize access to localization technologies, ensuring a broader spectrum of applications without compromising individual rights.

This dissertation proposal serves as a comprehensive blueprint for the forthcoming research on \gls{iol}.
It encapsulates the work carried out thus far, laying a solid foundation for the scholarly discourse.
The primary goal of this document is to provide the research trajectory, detailing the methodological approaches, theoretical frameworks, and empirical inquiries undertaken.
Furthermore, this proposal aims to provide a clear roadmap for what lies ahead, outlining the steps that will lead to the culmination of a dissertation.
By presenting both the groundwork already established and the tentative future directions, this proposal seeks to offer a holistic view of the research activities, ensuring clarity, coherence, and rigor in the pursuit of the insights into \gls{iol}.

\section{Literature Review}
Structural vibration-based occupant localization is a perception methodology where occupants' locations in an indoor environment are determined by analyzing the floor vibrations due to their footfall patterns.
Specifically, these methods employ the measurements of accelerometers that are fixed to the floor.
Henceforth, the terms vibro-localization and vibro-measurements will refer to such localization techniques and the measurements used in these techniques, respectively.
Vibro-localization techniques facilitate a myriad of applications ranging from smart home monitoring and event classification~\cite{Woolard2017, Li2020, Clemente2020} to human gait assessment~\cite{Fagert2017, Kessler2019, Kessler2019a, Davis2022} and occupant identification and tracking~\cite{Pan2015, Poston2017, Hu2021, Drira2022, Fagert2022}.

Energy-based vibro-localization techniques utilize the energy that is inherent in vibro-measurements as a localization feature because the signal energy serves as a consistent metric for gauging the magnitude of the vibro-measurements~\cite{Valero2021, Alam2021, MejiaCruz2021, davisdissertation}.
Specifically, higher signal amplitudes result in larger energy values registered by the sensors, and vice versa.
By employing this notion, energy-based vibro-localization techniques characterize the relationship between the signal energy and the length of the propagation path.
Therefore, these techniques offer a simplified approach to occupant localization, thereby reducing the need for exhaustive signal analysis.
In this study, the terms signal energy, power, intensity, and strength are used interchangeably despite their nuanced differences.

Dispersion is a natural phenomenon where different frequency components of structural waves propagate at different velocities in the medium, i.e., the floor.
This phenomenon has been seen as one of the major contributors to localization errors; hence, substantial scholarly endeavors have been directed toward examining the floor's dispersive attributes on localization outcomes.
These researchers have mitigated the dispersive effects inherent in the floor by isolating narrow frequency bands.
These bands, derived via Continuous Wavelet Transformation, remain unaffected by dispersion~\cite{Kwon2008, Racic2009, Ciampa2010, Mirshekari2016}.
\citet{Kwon2008}, for instance, presented a successful human activity recognition system utilizing floor vibro-measurements.
Their technique employed a feature extraction step based on wavelet packet decomposition coupled with statistical measures to capture the unique characteristics of different activities.
The empirical findings emphasized the efficacy of the proposed technique in the precise identification of diverse human activities.
%  highlighting its prospective utility in intelligent environments and surveillance infrastructures.
\citet{Racic2009} presented a technique for the detection and classification of human activities via floor vibrations.
They employed an approach involving a combination of wavelet-based feature extraction and a support vector machine classifier to accurately identify different activities, demonstrating the potential of floor vibro-measurements for activity monitoring and recognition in smart environments.
Such narrowband filtering essentially compromises the spatial resolution of the localization technique in hand; this challenge, especially in the context of radio-localization, has been discussed in detail (for instance, \cite{Ghany2020}).

The Warped Frequency Transformation technique has been also utilized to discern the dispersion curve, to mitigate perturbations attributed to dispersion~\cite{demarchi2011, woolard2018}.
There exist system-theoretic techniques that characterize the dynamic behavior of the floor via transfer function estimation~\cite{Mohammed2021, Davis2021, MejiaCruz2022}.
Additionally, the Green's function has been employed to account for wave reflections and its dispersion~\cite{Nowakowski2015}.
Despite their precision in empirical analyses, these techniques exhibit limitations in their capacity to generalize the complex material properties and boundary conditions inherent in floors.

On the other hand, \citet{bahroun2014} presented their work formalizing the group velocities, i.e., a major component of signal energy, as a function of propagation path distance.
These promising results paved the way for the model-based techniques which tend to explain the wave phenomena from the data.
\citet{alajlouni2018}, for instance, showed their hypothesis of an energy-decay model (energy logarithmically decays with propagation distance) as a localization model.
In their work, \citet{Pai2019} analyzed whether a relationship exists between the occupant's footfall patterns and the measured signal characteristics in an empirical case study.
In light of their work, the authors assert that there is no evidence of a monotonic relationship between the amplitude or kurtosis of the measured signal and the propagation distance.
Parametric energy-decay models highlight the exciting potential for further improvement~\cite{Alajlouni2020, Alajlouni2022, Wu2023, Tarrio2012}.

Along with parametric decay models, \citet{poston2016}, in an alternative attempt, moved the localization frameworks to a probabilistic framework by modeling the probability of detection and false alarm.
\citet{Alajlouni2022} took a similar approach to localize the occupant by maximizing the sensor likelihood functions given the hypothesis of the sensor's time domain measurements and the energy-decay model proposed in their earlier work.
\citet{Wu2023} propose G-Fall, a device-free fall detection system based on floor vibrations collected by geophone sensors.
Their system utilizes \glspl{hmm} and an energy-based vibro-localization technique, achieving precise and user-independent fall detection with a significant reduction in false alarm rates.

\section{Limitations of the Previous Literature}
One of the inherent challenges limiting the ability and success of vibro-localization techniques is that these techniques are single-shot estimators: each heel-strike and its corresponding vibro-measurement vector are unique; hence, repeated measurements for a single step are not easily attainable.
Therefore, most of the common estimation frameworks cannot be directly employed in such localization systems.
This challenge brings about the following limitations in the landscape of vibro-localization techniques.

\begin{enumerate}[label=L\arabic*.]
    \item \textbf{Limitations of Ideal Sensor Models}: Accelerometers are not ideal because they tend to introduce random and systematic errors in the vibro-measurement vector during signal acquisition time.
    While not trivial, the characterization of errors stemming from signal acquisition was not a central aspect of the existing literature.
    A complete understanding about the localization errors could not be achieved unless such sensor imperfections are categorically identified and incorporated into vibro-localization frameworks.
    \item \textbf{Limitations in Uncertainty Quantification}: The extent to which measurement errors contribute to localization errors is still unknown.
    In other words, the sensing errors in vibro-measurements are yet to be tied to the localization errors.
    It is imperative to account for errors in vibro-measurement vector for a successful localization technique.
    \item \textbf{Limitations in Information Reliability Assessment}: Along with the measurement imperfections, a myriad of uncertainty sources drive the success and failure cases of the energy-based vibro-localization techniques such as reflections, dispersion, etc.
    %  of the other disturbances, for instance, dispersion phenomenon, that can skew some sensors' measurements.
    To remedy the adverse effects of any unreliable sensor information at a given time, a metric needs to be proposed to measure the reliability of the information that each vibro-measurement vector carries.
\end{enumerate}

\section{Identified Objectives and Research Tasks}
Having identified the prevailing challenges and limitations in the existing literature related to indoor localization, especially in dynamic, unstructured indoor environments, it becomes imperative to delineate clear objectives for this dissertation. These objectives not only intend to bridge the gaps in the current knowledge but also aim to provide innovative solutions that can have practical implications in the realm of sensor networks and indoor localization.

The objectives laid out in the subsequent section are meticulously designed to address the multifaceted problems posed by indoor environments. By taking cues from the recognized limitations and the inherent complexities of such settings, this dissertation seeks to push the boundaries of current methodologies and introduce robust strategies for effective localization. These objectives also serve as a roadmap, guiding the trajectory of the research undertaken and ensuring alignment with the overarching aim of improving indoor localization techniques.
\subsection{Objectives}
\begin{enumerate}
    \item Develop a comprehensive model for accelerometer-based vibro-measurement that accounts for inherent sensor imperfections.
    \item Establish a correlation between sensing errors in vibro-measurements and the resultant localization errors.
    \item Propose a reliability metric that can evaluate the quality and trustworthiness of information carried by each vibro-measurement vector.
    \item Design an algorithm that makes use of sensor characteristics to identify the `Byzantine' member in the sensor network. 
    \item Derive a novel data-driven approach to learn distances from energy measurements.
    \item Given a set of vectors that may or may not consists multiple step measurements  of a set of sensors, blindly separate the signals into different events to localize these events.
\end{enumerate}

% \subsection{Research Tasks}
% \begin{enumerate}[label=T\arabic*.]
%     \item \todo{Task-1}:
%     \item \todo{Task-2}:
%     \item \todo{Task-3}:
% \end{enumerate}

\section{Summary of Achieved \& Anticipated Contributions}
This manuscript presents energy- and \gls{tdoa}-based vibro-localization techniques that address the sensor imperfections and their effects on the localization results.
The proposed techniques employ a family of accelerometers placed on a floor to generate multiple vibro-measurement vectors about some number of steps.
Furthermore, the one of proposed techniques employs two corrective steps during the localization time: (i) a comprehensive uncertainty quantification to minimize the effect of the internal errors occurring during the signal acquisition time present in the vibro-measurement vectors; and, (ii) an information-theoretic \gls{bse} algorithm to address the external sources of uncertainty such as reflections and dispersion.
The following points summarize the proposed technique's contributions.

\subsection{Achieved Contributions}
\begin{enumerate}[label=C\arabic*.]
    \item \textbf{Advancing the Understanding of Single-Sensor Vibro-Localization} (Addressing Limitations 1 and 2): \Cref{ch:imac} presents a detailed study on the uncertainties inherent in energy-based vibro-localization techniques, focusing specifically on single-sensor scenarios. This addresses a gap in existing research which predominantly focuses on multi-sensor systems.

    \item \textbf{Analytical and Numerical Analysis of Localization Error} (Addressing Limitations 2): \Cref{ch:imac} employs both theoretical (obtained via \gls{pdf} transformation theorem) and numerical methods (obtained via employing Taylor Series expansion on the mean localization error with \gls{lotus}) to estimate the mean localization error, considering factors like sensor noise and signal-to-noise ratio (SNR).

    \item \textbf{Vibro-localization Technique with Comprehensive Uncertainty Quantification} (Addressing Limitations 1 and 2): The proposed vibro-localization technique in \Cref{ch:sensors} employs an explicit error model for each sensor.
    Therefore, a complete uncertainty quantification on the localization errors due to the measurement errors can be minimized with our technique~\cite{s23239309}.

    \item \textbf{Information-Theoretic \gls{bse} Algorithm} (Addressing Limitation 3): \Cref{ch:sensors} introduces a \gls{bse} algorithm.
    The proposed \gls{bse} algorithm divides the sensors into two distinct subsets: the ones that show some consistency amongst them, and the ones which are divergent in nature.
    By leveraging a greedy information-theoretic approach, it decides whether a sensor should be placed in the former set, or vice versa.
    This algorithm guarantees a locally-optimal subset of the sensors in minimizing the localization errors.

    \item \textbf{Empirical Validation} (Addressing Limitations 1--3): Data from a previously conducted controlled experiments were employed to validate and benchmark the proposed technique~\cite{s23239309}.
    The results demonstrated significant improvements over the baseline~\cite{alajlouni2019new} approach in terms of both accuracy and precision.

    \item \textbf{Quantification of the Empirical Precision and Accuracy} (Addressing Limitation 3): \Cref{ch:sensors} employs the results of the empirical validation study to quantify an empirical correlation between precision and accuracy achieved with the proposed vibro-localization technique.
    By employing such correlation metrics, we gain better insights about the technique's performance and failures.
\end{enumerate}

\subsection{Anticipated Contributions}
\begin{enumerate}[label=C\arabic*.,resume]
    \item A signal multi-resolution decomposition methodology for the separation of vibration measurements into different events
    \item A feature extraction algorithm that yields features in pyramid structures
    \item A peak-detection algorithm based on the proposed signal decomposition methodology
    \item A non-maximal suppression algorithm to eliminate false-positives of the proposed peak-detection algorithm
    \item A wave velocity estimation technique with the proposed signal decomposition methodology
    \item Improved localization results with the detected peaks and estimated \glspl{tdoa}.
    \item An interval-detection algorithm based on the proposed signal methodology
    \item A robust bounding-box merging algorithm to eliminate false-positives of the proposed interval-detection algorithm
    \item Improved localization results with the energy of the detected intervals.
\end{enumerate}

\section{Timeline}
\Cref{fig:gantt-qual} details the scholarly activities completed by the student following the Ph.D. qualification exam, covering the period from April 2021 to October 2023.
Key milestones achieved during this time include attending two conferences (Conference-1 IMAC in February 2022 and Conference-2 SES in October 2022), and receiving the Torgersen Research Award in April 2023.
A significant focus was placed on Journal-1, with a timeline spanning from March to October 2023, involving stages of research, writing, and review, culminating in a submission in October 2023.
Notably, the chart also highlights changes in the student's academic journey, such as the previous advisor leaving Virginia Tech in July 2021, the commencement of work with Dr. Plassmann in December 2022, and the formation of a new committee in May 2023.

\input{figs/gantt_qual.tikz}

\Cref{fig:gantt-prelim} outlines the planned scholarly activities post-proposal exam, spanning from August 2023 to June 2024.
A key component is the ongoing work on Journal-2, which began in October 2023 and is expected to continue until April 2024.
This project is already in progress, with 60\% of the research and 40\% of the writing completed.
Another major focus is the dissertation, with an initial phase of writing (post-qualification) nearly completed (85\% progress) by January 4, 2024, and a subsequent phase (post-proposal) planned to start after the proposal exam on January 19, 2024.
The dissertation is set to conclude with a final exam on April 15, 2024, and an Electronic Thesis and Dissertation (ETD) submission by May 24, 2024.
A critical deadline marked on the chart is the last day to defend, scheduled for May 1, 2024.
\input{figs/gantt_post_prelim.tikz}

\section{Outline}
This proposal is organized into distinct chapters, each addressing essential aspects of indoor localization and the innovative use of sensor networks to measure floor vibrations.
Below is the overall outline of the proposal:

\begin{itemize}
    \item \textbf{\Cref{ch:introduction}: Introduction}
    This chapter sets the stage by outlining the primary challenges associated with localization in indoor environments. It establishes the significance of the research, its relevance in the current technological landscape, and introduces the reader to the broader context and objectives of the study.

    \item \textbf{\Cref{ch:imac}: Uncertainty Quantification of Energy-based Vibro-localization Technique} \\
    This chapter presents an in-depth examination of a unique vibro-localization technique. Emphasis is placed on its stochastic nature, the role of multi-sensor integration, and the novel concept of Byzantine sensor elimination. The methodology, experimental setup, results, and discussions pertaining to this technique are elaborated upon.

    \item \textbf{\Cref{ch:sensors}: A Multi-Sensor Stochastic Energy-based Vibro-localization Technique with Byzantine Sensor Elimination} \\
    This chapter presents an in-depth examination of a unique vibro-localization technique. Emphasis is placed on its stochastic nature, the role of multi-sensor integration, and the novel concept of Byzantine sensor elimination. The methodology, experimental setup, results, and discussions pertaining to this technique are elaborated upon.

    \item \textbf{\Cref{ch:paper2}: Localization of Multiple Occupants with Sigmoid Decomposition} \\
    This chapter introduces a novel signal decomposition technique, focusing on non-stationary and stochastic signals, particularly those exhibiting monotonic non-decreasing behavior.
    The proposed methodology, termed \gls{sd}, is designed to analyze signal energy or power accumulation over time, decomposing signals into sigmoid functions with varying weights and biases.
    This approach is particularly relevant in the context of monotonic non-decreasing functions, which are crucial in fields like probability theory and reliability engineering.

    The chapter delves into the application of \gls{sd} for decomposing monotonic non-decreasing signals from a power-centric perspective.
    It critiques traditional decomposition methods and proposes a new signal transform to better represent these types of signals.
    The method's ability to effectively filter out noise, especially white noise, is highlighted.

    The \gls{sd} process involves the construction of feature pyramids with weights, biases, and a masking vector, allowing for the layered representation and reconstruction of signals.
    The algorithm's computational complexity and the reconstruction accuracy depending on the number of layers used are discussed.
    Additionally, the chapter outlines a methodology for detecting significant events in signals using Non-Maximum Suppression.

    The chapter is structured to provide comprehensive coverage of \gls{sd}, from theoretical foundations to practical applications.
    It includes a literature review, detailed explanation of the technique, and outlines the methodology for experimentation.
    The chapter concludes by suggesting future research directions, emphasizing the potential for expanding and refining the \gls{sd} method.
\end{itemize}







