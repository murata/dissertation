\section{Introduction\label{sec:ch4_intro}}
Decomposition techniques in signal processing are pivotal for dissecting and understanding the intricate components of signals.
These techniques have evolved significantly from the early works of Fourier in the 1820s~\cite{Fourier1822} to contemporary advancements, enabling a nuanced analysis of complex signals.
In this paper, we propose yet another decomposition method that has a potential to tackle different signal processing problems such as signal segmentation and denoising, and event detection, etc. with signals that are non-stationary and stochastic in nature.
Our approach is centered around the analysis of how the energy or power of a signal accumulates over time.
\gls{sd} operates on this cumulative power metric as a proxy to the actual signal.
The proposed methodology, named \gls{sd}, breaks down this proxy representation of the signal in hand into a finite number of sigmoid functions with varying weights and biases.

Monotonic non-decreasing functions are essential across various scientific fields due to their property of consistent growth or stability.
In probability theory, for instance, the \gls{cdf} is a classic example of such function.
Real-world applications, such as reliability engineering and economic utility theory, often rely on the principles of non-decreasing functions.
This concept underpins logical consistency in models and analyses, making these functions critical for both theoretical and practical applications.

This paper explores the realm of signal decomposition through the lens of monotonic non-decreasing signals, analyzed from a power-centric perspective.
This paper aims to explore the intricate world of signal decomposition methodologies, with a specific focus on monotonic non-decreasing signals from a power-centric perspective.
The study delves into the challenges and limitations posed by traditional methods, seeking to address specific gaps identified in the current literature.
In doing so, it presents a novel signal transform that can represent the the monotonic non-decreasing signals better than the contemporary work.

\subsection{Problem Statement}
The study of monotonic non-decreasing sequences in signal decomposition offers a unique viewpoint for understanding power accumulation in signals, especially in non-stationary and stochastic contexts.
This perspective is vital for interpreting the inherent nature and behavior of signals, which traditional decomposition methods might obscure.
\Cref{dft:monotonic} provides a formal definition of such sequences.

\begin{definition}[Monotonic Non-Decreasing Discrete Sequences]
    \label{dft:monotonic}
    Let \( x: \mathcal{N} \rightarrow \mathbb{R} \) be a monotonic non-decreasing sequence defined over a regularly spaced set of points shown as \(\mathcal{N} = \{0, 1, \ldots, N-1\}\).
    A sequence is said to be a monotonic non-decreasing sequence if for every pair of indices \( i, j \in \mathcal{N} \) with \( i \leq j \), it holds that \( x[i] \leq x[j] \).
    In other words, the value of the sequence at a later index is never less that of an earlier index.
    This condition ensures that the sequence does not decrease as the index increases.
    Additionally, the sequence is bounded if \( y_0 \leq x[n] \leq y_1 \), where \( y_0, y_1 \) are real numbers, meaning that the values of the sequence are confined within the range defined by \( y_0 \) and \( y_1 \).
\end{definition}

Decomposition techniques in signal processing vary widely in their characteristics and applicability. The effectiveness of a particular method depends on the signal's nature and the specific requirements of the analysis.
While some techniques are better suited for certain types of signal breakdowns, others might excel in computational efficiency or provide unique insights.
Our proposed signal decomposition methodology is designed to fulfill the need for an effective decomposition method for signals characterized by non-stationary behavior.
However, all signal decomposition methodology are expected to share the loose definitions shown in \Cref{dft:decomposition,dft:reconstruction}.

\begin{definition}[Decomposition of Signals]
    \label{dft:decomposition}
    Signal decomposition in signal processing is the process of breaking down a signal into its constituent components, simplifying its analysis, interpretation, or further processing. Representing the space of original signals as \( S \), a decomposition operation \( \mathcal{D} \) can be defined as a mapping:

    \[ \mathcal{D}: S \rightarrow \{C_0, \ldots, C_{n-1}\}~, \]

    where each \( C_i \) is a component of the original signal \( s \in S \). These components represent different aspects or characteristics of the signal, varying with the decomposition method.
\end{definition}

Following the definition of signal decomposition \(\mathcal{D}\), the concept of reconstruction or synthesis is considered. This process involves reassembling the decomposed components back into a signal form, which is essential in applications where the original signal's holistic representation is needed post-decomposition.

\begin{definition}[Reconstruction of Signals]
    \label{dft:reconstruction}
    Signal reconstruction, denoted as \(\mathcal{D}^{-1}\), is the process of reassembling the components of a decomposed signal back into its original form. This operation can be viewed as a mapping:

    \[ \mathcal{D}^{-1}: \{C_0, \ldots, C_{n-1}\} \rightarrow S \]

    where a set of components \( \{C_0, \ldots, C_{n-1}\} \) is transformed back into the original signal \( s \in S \). The integrity of a signal post-analysis is ensured through accurate reconstruction from its components.

    The feasibility and exact methodology of the reconstruction process \(\mathcal{D}^{-1}\) depend on the nature of the decomposition \(\mathcal{D}\) and the properties of the components and the original signal space.
\end{definition}

\subsection{Summary of Contributions}
Our primary contribution is the development of the \gls{sd}, a method tailored for decomposing monotonic non-decreasing signals. Unlike conventional decomposition methods, the \gls{sd} effectively discards noise, particularly white noise, inherent in signals, owing to the properties of its sigmoid components.
\st{Because the proposed transform is linear, superposition and other advantages of linear transforms are contained in the Discrete Sigmoid Transform.}
\todo{I need to revisit this.}

\subsection{Outline}
The paper is structured to provide a detailed exploration of the \gls{sd} and its applications in signal processing.
Following this introduction, \Cref{sec:ch4_literature} presents a Literature Review, placing our work within the context of existing research.
In \Cref{sec:ch4_theory}, we introduce the concept and theoretical framework of the \gls{sd}.
\Cref{sec:ch4_methodology} outlines our methodology, including experimental design and data analysis.
\Cref{sec:ch4_results} presents our findings, followed by a discussion in comparison with established theories.
The paper concludes in \Cref{sec:ch4_conclusion}, summarizing our research's broader impact and suggesting future research directions, including potential expansions and localization studies.
This section also sketches out avenues for future work, proposing potential expansions of our research and suggesting topics for further investigation.
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\section{Previous Works\label{sec:ch4_literature}}
\gls{emd} is an adaptive and data-driven analysis method extensively used in signal processing.
\gls{emd} decomposes a signal into a finite set of components called \glspl{imf}~\cite{huang1998empirical}.
Each \gls{imf} represents a simple oscillatory mode akin to a simple harmonic function but more flexible.
The method is particularly effective for analyzing non-linear and non-stationary data, common in real-world signals.
The process involves iteratively extracting \glspl{imf} from the signal. Each IMF is identified through a sifting process, ensuring that it exhibits specific properties, such as having symmetric envelopes defined by local maxima and minima, and zero mean.
The original signal is progressively decomposed into these \glspl{imf}, plus a residue that represents the trend in the data.
This decomposition allows for a more nuanced analysis of the signal, enabling the extraction of meaningful information that traditional linear methods might overlook.
Huang's paper laid the foundation for EMD and remains a vital reference for understanding and applying this technique in signal processing. The method has since evolved, with various modifications and improvements proposed in subsequent research, continually broadening its applicability and effectiveness in signal analysis.

\gls{ica} is a computational method for separating a multivariate signal into additive, independent non-Gaussian components \cite{hyvarinen2000independent}.
ICA assumes that the observed multivariate data are linear mixtures of some unknown latent variables, and the mixing system is also unknown. The primary goal of \gls{ica} is to recover the independent sources given only the mixed signals.
The standard procedure of \gls{ica} begins with centering the data, which involves subtracting the mean from each variable, ensuring the data is centered around zero. 
his is followed by whitening or sphering the data to make the signals uncorrelated and their variances equal to one.
Whitening simplifies the subsequent estimation process as it reduces the number of parameters to be estimated.
Once the data is whitened, the \gls{ica} algorithm iteratively estimates the independent components.
This process typically involves optimizing a function that measures the non-Gaussianity of the components, as the central idea behind \gls{ica} is that the more non-Gaussian a component is, the more likely it is to be independent.
Common measures of non-Gaussianity include kurtosis, negentropy, or mutual information.
The algorithm adjusts the weights in a manner that maximizes the non-Gaussianity of the output, thus separating the independent components.

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\section{Sigmoid Decomposition\label{sec:ch4_theory}}
\gls{sd}, denoted as \( \mathcal{S} \), is a multi-scale signal analysis methodology which yields structurally-layered features for a given sensor measurement.
Let \( \vect{x} \) be the measurement vector containing the sensor readings for a duration of time.
In this work, we denote the measurement vector as a column vector \( \vect{x} = \left( x[0], \ldots, x[N-1] \right)^\top \in \mathbb{R}^N \).
Here, \( x[n] \in \mathbb{R} \) denotes a discrete, real, bounded, non-stationary, and, stochastic process representing measurements acquired from a sensor at time steps \( n \in \mathcal{N} = \{0, \ldots, N-1\} \).
The set of time instances \( \mathcal{N} \) encompasses a sequence of discrete, regularly sampled time instances, represented as integers in sequential order.
\subsection{Signal Decomposition}
Let \( p[n] = \sum_{i}^{n} x[n]^2 \) be the cumulative power of the signal \( x[n] \) up to sample $n$ showing how signal power accumulates over time.
\gls{sd} originates from the observation that the cumulative power of a measurement vector $\vect{x}$ yields a monotonic non-decreasing function.
\gls{sd} uses this representation of signal \( x[n] \), as the proxy and decomposes it as weighted and shifted sum of discrete sigmoid function given as \( \sigma[n] = \frac{1}{1 + e^{-x}} \).
\Cref{eq:cumulative-power} below provides this relationship.

\begin{equation}
    p[n] = \sum_{i=0}^{n}{x[i]^2} = \sum_{i=0}^{N-1} w_i \sigma \left[ \frac{n-b_i}{\kappa} \right]
    \label{eq:cumulative-power}
\end{equation}
Notice, \( p[n] \) forms a monotonic non-decreasing function which makes it ideal sigmoid function to approximate it.
The discrete sigmoid function, serving as the kernel for our decomposition, is defined on \(\sigma: \mathbb{Z} \rightarrow [0, 1] \), and, is bounded and monotonically increasing on \( \mathbb{Z} \).
It approaches 0 as \( n \) becomes large and negative, and approaches 1 as \( n \) becomes large and positive.

\gls{sd} is able the measurement vector \( \vect{x} \) into \( K \) number of layers.
Each layer that is characterized by a weight vector \( \vect{w}_i \) and a bias vector \( \vect{b}_i \) is sufficient to reconstruct the signal $x[n]$ back.
At the \( i^{th} \) level of feature pyramid, the signal $x[n]$ is decomposed into $i+1$ sigmoids whose weights $\vect{w}_i \in \mathbb{R}^{i+1}_{+}$ and biases $\vect{b}_i \in \mathbb{R}^{i+1}_{+}$ where $i \in \mathcal{K} = \{0, \ldots, N - 1 \}$.
Because each layer provides different number of components, \gls{sd} yields two $K$-level feature pyramids (or sets) $\mathcal{W} \triangleq \left\{\vect{w}_0, \ldots, \vect{w}_{N-1} \right\}$,  $\mathcal{B} \triangleq \left\{\vect{b}_0, \ldots, \vect{b}_{N-1} \right\}$, and a masking vector $\vect{m} \in \mathbb{R}^N$.
The \gls{sd} is formally shown as in \Cref{eq:sd}.
\begin{equation}
    \mathcal{S}: \vect{x} \rightarrow \{ \mathcal{W}, \mathcal{B}, \vect{m} \}~.
    \label{eq:sd}
\end{equation}

The pyramids $\mathcal{W}$ and $\mathcal{B}$ are the weights and biases of the sigmoid functions, while the mask $\vect{m}$ is used to tackle the sign ambiguity when signal is reconstructed from its decomposition.
The masking vector is defined as the sign of the signal \( \vect{m} \triangleq \sign{\left( \vect{x} \right)} \).
A graphical representation of the feature pyramids are given in \Cref{fig:pyramids}.
The figure demonstrates the fact that as the pyramid gets deeper, the temporal resolution increases with increasing number of sigmoid functions.
On the other hand, as the pyramid gets deeper, signals semantics becomes vague.

\input{figs/pyramid.tikz}

\Cref{alg:sd} gets the input signal $\vect{x}$ as the argument and yields the complete weight $\mathcal{W}$ and bias $\mathcal{B}$ pyramids, as well as the masking vector $\vect{m}$.
The algorithm starts by calculating the cumulative power of the input vector $\vect{x}$.
Consequently, for each layer of the pyramid indexed with $k$, the calculated cumulative power is split into $k+1$ equal length segments.
If the length of the signal is not divisible by the layer index, the cumulative power of the signal is appended with its last member to remedy this.
Note that this appendange is equivalent to adding zeros at the end of the signal $x[n]$.
The weights and biases concerning each segment is then calculated as follows:
For each segment indexed with $i$, we set the corresponding element of weight vector with the total increase observed in the segment.
As for the bias, the time index that maximizes the derivative of the cumulative power $p[n]$.
By doing so, we place the sigmoid of this segment at the point where maximum change, likely corresponding to an event, is observed.

\begin{algorithm}[ht]
    \caption{Sigmoid Decomposition (SD)}
    \label{alg:sd}
    \begin{algorithmic}[1]
    \Require Signal $x[n]$, $n \in \mathcal{N} = \{0, \ldots, N-1\}$
    \Ensure Feature Pyramids $\mathcal{W}, \mathcal{B}$ and Masking Vector $\vect{m}$

    \Function{SD}{$x[n]$}
        \State Compute the cumulative power of $x[n]$: $p[n] \gets \sum_{i=0}^{N-1} \lvert x[i] \rvert^2$
        \State Initialize feature pyramids: $\mathcal{W} \gets \{\}, \mathcal{B} \gets \{\}$
        \For{$k \gets 0$ \textbf{to} $N-1$}
            \State Let $\Delta = \frac{N}{k+1}$
            \State Initialize weights and biases: $\vect{w}_k \gets \vect{0}_{k+1}, \vect{b}_k \gets \vect{0}_{k+1}$
            \For{$i \gets 0$ \textbf{to} $k$}
                \State $increase \gets p[\lfloor (i+1) \Delta \rfloor] - p[\lfloor i \Delta \rfloor]$
                \State $\vect{w}_k[i] \gets increase$
                \If{$increase > 0$}
                    \State $\vect{b}_k[i] \gets \argmax_{n \in \{\lfloor i \Delta \rfloor, \ldots, \lfloor (i+1) \Delta \rfloor \}}{\frac{\diff}{\diff n}p[n]}$
                \Else
                    \State $\vect{b}_k[i] \gets \lfloor \Delta (i + \frac{1}{2})\rfloor$
                \EndIf
            \EndFor
            \State $\mathcal{W} \gets \mathcal{W} \cup \{\vect{w}_k\}$
            \State $\mathcal{B} \gets \mathcal{B} \cup \{\vect{b}_k\}$
        \EndFor
        \State Compute masking vector: $\vect{m} \gets \sign{(x[n])}$
        \State \Return $(\mathcal{W}, \mathcal{B}, \vect{m})$
    \EndFunction
    \end{algorithmic}
\end{algorithm}

\input{figs/signals.figure}

\begin{remark}
    As can be seen in the \Cref{alg:sd}, the \gls{sd} yields a redundant, i.e. over-complete, representation of the signal $x[n]$.
    If algorithm performance is a concern, as in real-time processing, the pyramids can be calculated for a range of layers, even a single layer.
\end{remark}

\begin{remark}[Computational Complexity of the \gls{sd}]
    As can be seen in \Cref{alg:sd}, the computational complexity of constructing $n^{th}$ layer of the pyramids is $O(n)$.
    If full pyramids are constructured, the overall performance of the algorithm is $O(N^2)$. 
\end{remark}

\subsection{Signal Reconstruction}
The process of reconstructing the original signal \( \vect{x} \) from the \gls{sd} involves utilizing the feature pyramids \( \mathcal{W} \) and \( \mathcal{B} \), along with the masking vector \( \vect{m} \). The inverse transformation, denoted as \( \mathcal{S}^{-1} \), aims to reverse the effects of the \gls{sd}, combining the components represented in the feature pyramids to approximate the original signal.

\begin{equation}
    \mathcal{S}^{-1}: \{ \mathcal{W}, \mathcal{B}, \vect{m} \} \rightarrow \vect{x}~.
    \label{eq:sd-inverse}
\end{equation}

The reconstruction process from a single layer $k$ can be expressed as follows:
\begin{equation}
    \hat{p}[n] = \sum_{i=0}^{k-1} \vect{w}_k[i] \cdot \sigma \left[ \frac{n - \vect{b}_k[i]}{\kappa} \right]
    \label{eq:reconstruction-pn}
\end{equation}

\begin{equation}
    \hat{x}[n] = \sqrt{ \hat{p}[n] - \hat{p}[n-1] } \cdot m[n]
    \label{eq:reconstruction}
\end{equation}

In Equation \ref{eq:reconstruction}, \( \vect{w}_k[i] \) and \( \vect{b}_k[i] \) represent the weight and bias of the \( i^{th} \) sigmoid component in the \( k^{th} \) layer of the feature pyramid, respectively. The function \( \sigma \) is the sigmoid function, and \( \kappa \) is a scaling factor which adjusts the steepness of the sigmoid. The masking vector \( \vect{m} \) is used to restore the original sign of the signal, as well as to handle the absolute value taken during the square root operation.

\begin{remark}
    The reconstruction accuracy depends on the number of layers and components used in the SD. Higher layers and components lead to a more accurate reconstruction but at the cost of increased computational complexity.
\end{remark}

\begin{remark}
    The reconstruction process, as outlined in Equation \ref{eq:reconstruction}, assumes an ideal scenario. In practical applications, some information loss during decomposition might lead to minor discrepancies between the original and reconstructed signals.
\end{remark}
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\section{Methodology\label{sec:ch4_methodology}}
\subsection{Occupant Localization Using \gls{sd}}
In our approach, we employ \gls{sd} for the task of occupant localization in a monitored environment.
The methodology involves decomposing sensor signals into their constituent sigmoid components, which allows for the precise identification of signal events and characteristics.
This decomposition is particularly beneficial in cases with non-stationary and stochastic signals, where conventional methods struggle.
The \gls{sd} process analyzes the cumulative power of the signals, using the sigmoid functions' varying weights and biases to represent different signal aspects.
By isolating specific signal components corresponding to different events, we can accurately detect and localize occupancy-related events, such as movements or presence in a particular area.

The localization algorithm operates on the premise that distinct occupant activities generate unique signal patterns, which can be identified through the \gls{sd} process.
Each activity results in a specific set of sigmoid components that are characterized by their unique weights and biases.
By analyzing these components, the system can determine the type and location of the occupancy event.
This process not only enhances the accuracy of localization but also improves the system's ability to differentiate between different types of occupancy-related events.

\subsection{Peak Detection and Non-maximum Supression Algorithm with Sigmoid Decomposition}
The \gls{nms} algorithm plays a crucial role in our occupant localization system.
It functions by processing the outputs of the \gls{sd} to identify significant events in the sensor data.
The algorithm starts by evaluating each component produced by the \gls{sd}, assigning a confidence level based on the component's weight relative to the maximum weight in its layer.
This process correlates high confidence events with the peaks in the signals.
Events with confidence levels above a predetermined threshold are considered for further processing.
Each event is represented with a unique id and attributes such as time, energy, and confidence level.
\Cref{alg:nms} provides the proposed algorithm.

The core of the \gls{nms} algorithm lies in its ability to filter out overlapping events that are temporally close, ensuring that only the most significant events are considered for localization.
This is achieved by sorting all detected events by their confidence levels and comparing each event against already selected significant events.
If two events are temporally close, the one with the higher confidence level is retained. This approach effectively reduces noise and false positives, providing a cleaner set of data for accurate occupant localization.

\begin{algorithm}
    \caption{Non-Maximum Suppression for Sensor Events}
    \label{alg:nms}
    \begin{algorithmic}[1]
    \Require Signal $x[n]$, $n \in \mathcal{N} = \{0, \ldots, N-1\}$
    \Ensure Detected events $\mathcal{E}$
    \State \textbf{Class:} DetectionObject
    \State \textbf{Attributes:}
    \State \ \ \ \ $time$: Time of the event
    \State \ \ \ \ $energy$: Energy of the event
    \State \ \ \ \ $confidence$: Confidence level of the event
    \State \Comment{The DetectionObject represents a detected event with its attributes.}

    \Function{NMS}{$x[n]$}
        \State Initialize $\mathcal{E} \gets \{ \}$ \Comment{Significant events}
        \State Initialize $\mathcal{D} \gets \{ \}$ \Comment{All detected events}1
        \State $\mathcal{W}, \mathcal{B}, \vect{m} \gets \Call{SD}{x[n]}$
        \For{each pair $(\vect{w}, \vect{b})$ in $(\mathcal{W}, \mathcal{B})$}
            \For{each pair $(w, b)$ in $(\vect{w}, \vect{b})$}
                \State $c \gets w / \Call{Max}{\vect{w}}$ \Comment{Confidence of a component in a layer}
                \If{$c > confidence\_thr$} \Comment{If confidence is higher than a threshold (0, 1)}
                    \State $e \gets$ \Call{DetectionObject}{$b, x[b]^2, c$}
                    \State $\mathcal{D} \gets \mathcal{D} \cup \{ e \}$
                \EndIf
            \EndFor
        \EndFor
        \State Sort $\mathcal{D}$ by confidence in descending order
        \For{each event $c$ in $\mathcal{D}$} \Comment{$c$: Current Event from Detected events}
            \For{each event $s$ in $\mathcal{E}$} \Comment{$s$: Significant Event}
                \If{$\lvert c.time - s.time \rvert \leq temporal\_thr$} \Comment{the events are temporally close}
                    \If{$c.confidence \geq s.confidence$} \Comment{$c$ is more confident than $s$}
                        \State $\mathcal{E} \gets (\mathcal{E} \setminus \{s\}) \cup \{c\}$ \Comment{Replace $s$ event with $c$}
                    \EndIf
                \EndIf
            \EndFor
        \EndFor
        \State \Return Detected event set $\mathcal{E}$
    \EndFunction
    \end{algorithmic}
\end{algorithm}

\subsection{\gls{tdoa} Localization with Detected Peaks}
The success of \gls{tdoa} localization heavily relies on the accuracy of the \gls{nms} results.
\gls{tdoa} techniques depend on the precise detection of signal events' timing to calculate the time differences as signals reach various sensors.
These time differences are then used to triangulate the location of the event source, such as an occupant in a building. Therefore, the precision of event detection, ensured by effective \gls{nms} filtering, is critical.
Accurate \gls{nms} results lead to precise \gls{tdoa} calculations, which are fundamental for reliable occupant localization.

The \gls{tdoa} localization process begins once the \gls{nms} algorithm has identified the most significant events. By measuring the time differences in the arrival of a signal at various sensors, the algorithm calculates the source's location using triangulation methods.
This process requires a well-synchronized sensor network and a clear understanding of the sensors' spatial configuration.
The accuracy of \gls{tdoa} localization is directly influenced by the precision of the event timing identified by the \gls{nms} algorithm.
Therefore, the integration of accurate \gls{nms} results with \gls{tdoa} techniques forms a robust framework for occupant localization, essential for various applications in smart building management and security.
% %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\section{Experimental Validation and Preliminary Results \label{sec:ch4_results}}
The experimental setup involves a thin acrylic plate of dimensions 300mm x 300mm and 4mm thickness, segmented into 25 identical square tiles of 60mm x 60mm.
Four \glspl{pzt}, labeled as A, B, C, and D, are bonded to the corners of select tiles.
The tiles are arranged in a grid, represented as \( \mathbf{Q} = \{q_{1,1}, q_{1,2}, \ldots, q_{5,5}\} \), where each tile \( q_{x,y} \) is defined by its row \( x \) and column \( y \).
The placement of the \glspl{pzt} is as follows: \gls{pzt} A on \( q_{1,1} \), \gls{pzt} B on \( q_{1,5} \), \gls{pzt} C on \( q_{5,5} \), and \gls{pzt} D on \( q_{5,1} \).
An impact event is simulated by dropping a steel ball (9.5 mm diameter, 3.53 gr weight) from heights varying between 10 cm and 20 cm in 0.5 cm intervals onto the plate.

For monitoring, the \glspl{pzt} are connected to an Arduino NANO 33 BLE microcontroller, chosen for its memory capacity and processor speed, suitable for TinyML applications.
The microcontroller's inability to read negative values is addressed by incorporating a voltage divider between the sensors and the device.
This setup shifts the reference voltage of the idle state to a value greater than zero, facilitating data acquisition.
When an impact occurs, the structure flexes, producing both positive and negative output voltages from the \glspl{pzt}.
The voltage divider thus enables the microcontroller to effectively capture the impact-induced signal variations for subsequent processing and impact localization.

\input{figs/katsidimas.figure}
% %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\section{Conclusions \& Future Work\label{sec:ch4_conclusion}}

