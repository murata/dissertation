import argparse
import simulation as sim
import matplotlib.pyplot as plt
import numpy as np


def video(settings):
    data = np.load('finite.npz')

    # Extract the arrays
    energies = data['energies']
    times = data['times']
    states = data['states']
    spatial_domain = data['spatial_domain']
    temporal_domain = data['temporal_domain']
    video_times = sim.video_times(settings["temporal_properties"]["total_time"], settings["video"]["fps"], times[0])
    print(video_times)

if __name__ == '__main__':
    parser = argparse.ArgumentParser(description="Read a JSON file and process its contents.")
    parser.add_argument("file_path", type=str, help="Path to the JSON file")
    args = parser.parse_args()
    settings = sim.read_json_file(args.file_path)

    video(settings)
