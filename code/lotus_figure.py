import numpy as np
import matplotlib.pyplot as plt
import scipy.stats as ss
import seaborn as sns 

def fit_line(x, y):
    # Fit a polynomial of degree 1 (a line) to the data
    slope, intercept = np.polyfit(x, y, 1)

    return slope, intercept

def evaluate_line(x, slope, intercept):
    return x * slope + intercept

def delta_func(epsilon, beta=-1, e=1):
    return -1/beta * np.log(1 + epsilon/e)

def delta_func_inverse(y, beta=-1, e=1):
    # Assuming the inverse function is provided
    # Placeholder function, replace with actual inverse
    return e * (np.exp(-beta * y) - 1)

def f_epsilon(x):
    return ss.norm.pdf(x, loc=1.5, scale=0.3)

# Define the derivative of the inverse function
def derivative_delta_func_inverse(y, beta=-1, e=1):
    # Assuming the derivative is provided
    # Placeholder function, replace with actual derivative
    return beta * e * np.exp(-beta * y)

def main():
    n = 100
    epsilon = np.linspace(0, 4, n)
    chi = delta_func(epsilon)
    chi_linear = evaluate_line(epsilon, *fit_line(epsilon, chi))
    fepsilon = ss.norm(loc=1.5, scale=0.3).pdf(epsilon)
    fchi = f_epsilon(delta_func_inverse(chi)) * np.abs(derivative_delta_func_inverse(chi))

    g = sns.JointGrid(x=epsilon, y=chi)
    g.ax_joint.plot(epsilon, chi, "k-.")
    # g.ax_joint.minorticks_on()
    # g.ax_joint.grid(which="both")
    g.ax_joint.set_xlim([0, 4])
    g.ax_joint.set_ylim([0, np.max(chi)])
    g.ax_joint.set_xlabel(r"$\varepsilon$")
    g.ax_joint.set_ylabel(r"$\chi = \Delta(\varepsilon)$")
    g.ax_marg_x.plot(epsilon, fepsilon, "r-")
    g.ax_marg_x.set_xlim([0, 4])
    g.ax_marg_y.plot(fchi, chi, "r-")
    g.ax_marg_y.set_xlim([0, 4])
    g.fig.set_size_inches(3, 3)  # Width, Height in inches
    g.fig.set_dpi(300)
    g.fig.tight_layout()
    g.fig.savefig("../presentation-prelim/figures/lotus-joint.png")


if __name__ == '__main__':
    main()
