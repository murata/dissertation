import os
import json
import time
import functools
import natsort
from PIL import Image
import numpy as np
import scipy.signal as ss
import scipy.linalg as sl
from moviepy.editor import ImageSequenceClip
import matplotlib.pyplot as plt

def timeit(func):
    """Decorator to log the function's module, name, and execution time."""

    @functools.wraps(func)
    def wrapper(*args, **kwargs):
        start_time = time.time()
        result = func(*args, **kwargs)
        end_time = time.time()
        
        module_name = func.__module__
        func_name = func.__qualname__
        elapsed_time = end_time - start_time

        print(f"Executed {module_name}.{func_name} in {elapsed_time:.4f} seconds")
        return result

    return wrapper

def video_times(total_time, fps, temporal_domain):
    x = np.linspace(0, total_time, int(total_time * fps))
    return [np.argmin(np.abs(temporal_domain - t)) for t in x]


def prepare_figures(ts, xs, vid_times, force_locations, sensor_locations, spat_doms, temp_doms, xlims, ylims):

    fig, axes = plt.subplots(len(ts), 3, figsize=(14, 10), dpi=600)
    for idx in vid_times:
        for ax, t, x, f_loc, s_loc, spat, temp, xlim, ylim in zip(axes, ts, xs, force_locations, sensor_locations, spat_doms, temp_doms, xlims, ylims):
            ax.cla()
            ax = plot_ax(ax, idx, x, f_loc, s_loc, spat, temp, xlim, ylim)
    fig.savefig(f"folder/frame-{idx:03d}.png", dpi=600)

def plot_ax(ax, t_idx, x, force_location, sensor_location, spatial_domain, temporal_domain, xlim, ylim):
    ax.plot(spatial_domain, x, * 100)
    sensor_location_idx = [np.argmin(np.abs(spatial_domain)- x for x in sensor_location)]
    force_location_idx = np.argmin(np.abs(spatial_domain - force_location))
    for idx, loc in zip(sensor_location_idx, sensor_location):
        ax.scatter(loc, x[t_idx, idx], color="red", label="Sensor")
    ax.scatter(force_location, x[t_idx, force_location_idx], "rx", legend="Impact")
    ax.set_xlim(xlim)
    ax.set_ylim(ylim)
    ax.set_xlabel("Spatial Domain [m]")
    ax.set_ylabel("Displacement [cm]")
    ax.set_title(f"Time: {temporal_domain[t_idx]:0.2f} [s]")
    return ax

@timeit
def get_sorted_image_files(foldername, filename_format):
    # Find and sort files
    files = [f for f in os.listdir(foldername) if f.endswith(filename_format)]
    sorted_files = natsort.natsorted(files)
    return [os.path.join(foldername, file) for file in sorted_files]

@timeit
def create_gif(foldername, filename_format, fps):
    sorted_image_files = get_sorted_image_files(foldername, filename_format)

    # Read images
    images = [Image.open(file) for file in sorted_image_files]

    # Create GIF
    frame_duration = 1000 / fps  # duration per frame in milliseconds
    gif_filename = os.path.join(foldername, 'output.gif')
    images[0].save(gif_filename, save_all=True, append_images=images[1:], duration=frame_duration, loop=0)
    print(f"GIF created: {gif_filename}")

@timeit
def create_mp4(foldername, filename_format, fps):
    sorted_image_files = get_sorted_image_files(foldername, filename_format)

    # Load images into moviepy
    clip = ImageSequenceClip(sorted_image_files, fps=fps)

    # Write to file
    mp4_filename = os.path.join(foldername, 'output.mp4')
    clip.write_videofile(mp4_filename)
    print(f"MP4 video created: {mp4_filename}")

def read_json_file(file_path):
    """
    Reads a JSON file and returns its contents as a dictionary.

    Parameters:
    file_path (str): The path to the JSON file.

    Returns:
    dict: Contents of the JSON file.
    """
    try:
        with open(file_path, 'r') as file:
            data = json.load(file)
            return data
    except Exception as e:
        print(f"An error occurred while reading the file: {e}")
        return {}

@timeit
def generate_model(fun, spatial_domain, spatial_resolution, force_location, density=8000, tension=150, damping_ratio=0.1):
    dx = spatial_resolution

    M, K = generate_matrices(dx, len(spatial_domain), density, tension)
    M, K = fun(M, K)
    C = damping_ratio * K
    wave_speed = np.sqrt(tension / density)
    natural_frequencies = calculate_natural_frequencies(M, K)
    nyquist_frequency = 2 ** np.ceil(np.log2(np.amax(natural_frequencies) * 2))
    print(f"Wave Speed: {np.round(wave_speed, 2)} [m \cdot s^-1]")
    system = generate_state_space(M, K, C, force_location, nyquist_frequency)

    return system, nyquist_frequency

@timeit
def generate_matrices(length, num_elements, density, tension):
    element_length = length / num_elements

    M = np.zeros((num_elements + 1, num_elements + 1))
    K = np.zeros((num_elements + 1, num_elements + 1))

    # Populate the mass and stiffness matrices
    for i in range(num_elements):
        M[i, i] += density * element_length / 3
        M[i, i + 1] += density * element_length / 6
        M[i + 1, i] += density * element_length / 6
        M[i + 1, i + 1] += density * element_length / 3

        K[i, i] += tension / element_length
        K[i, i + 1] -= tension / element_length
        K[i + 1, i] -= tension / element_length
        K[i + 1, i + 1] += tension / element_length

    return M, K

def string_fea_finite(M, K):
    K[0, :] = K[:, 0] = K[-1, :] = K[:, -1] = 0
    K[0, 0] = K[-1, -1] = 1
    M[0, :] = M[:, 0] = M[-1, :] = M[:, -1] = 0
    M[0, 0] = M[-1, -1] = 1
    return M, K

def string_fea_infinite(M, K):
    return M, K

@timeit
def generate_state_space(M, K, C, force_location, fs):
    """
    Generate state-space representation matrices A, B, C, D from the system's matrices.

    Parameters:
    - K, M, C (numpy.ndarray): Stiffness, mass, and damping matrices.
    - force_location (int): Index of the force application point.

    Returns:
    - A, B, C, D (numpy.ndarray): State-space matrices.
    """
    n_dof = M.shape[0]  # Number of degrees of freedom

    # Construct the A matrix
    A = np.zeros((2 * n_dof, 2 * n_dof))
    A[:n_dof, n_dof:] = np.eye(n_dof)
    A[n_dof:, :n_dof] = -np.linalg.inv(M) @ K
    A[n_dof:, n_dof:] = -np.linalg.inv(M) @ C

    # Construct the B matrix
    B = np.zeros((2 * n_dof, 1))
    B[n_dof + force_location, 0] = 1

    # Construct the C matrix
    C = np.zeros((1, 2 * n_dof))
    D = np.zeros((1, 1))
    return ss.StateSpace(A, B, C, D).to_discrete(1/fs)

@timeit
def simulate(state_space, force, time):
    return ss.dlsim(state_space, u=force, t=time)

@timeit
def calculate_natural_frequencies(M, K):
    eigenvalues, _ = sl.eigh(K, M)
    if np.any(np.isinf(eigenvalues)):
        raise ValueError("Eigen-values are not finite")
    
    if np.any(np.isnan(eigenvalues)):
        raise ValueError("NaN encountered in egienve values")

    natural_frequencies = np.sqrt(np.abs(eigenvalues))
    return natural_frequencies

def argmax2d(array, x, y):
    # Find the index of the maximum value
    u, v = np.unravel_index(np.argmax(array), array.shape)

    # Get the maximum value
    max_value = array[u, v]

    return max_value, np.array((u, v)), np.array((y[u], x[v]))

@timeit
def simulate_string(func, force_location, spatial_properties, temporal_properties, material_properties):
    # Extracting spatial properties
    length = spatial_properties['length']
    dx = spatial_properties['dx']
    num_elements = int(length / dx)
    spatial_domain = np.linspace(0, length, num_elements)
    spatial_resolution = 1 / num_elements
    print(f"Spatial Resolution: {spatial_resolution:0.2f} [m] yielded, {num_elements} elements")
    force_location_id = np.argmin(np.abs(spatial_domain - force_location))

    # Extracting temporal properties
    frequency_ratio = temporal_properties['frequency_ratio']
    impact_time = temporal_properties['impact_time']
    total_time = temporal_properties['total_time']

    # Extraction material properties
    density = material_properties["density"]
    tension = material_properties["tension"]
    damping_ratio = material_properties["damping_ratio"]
    # Generating the system model
    system, nyquist_freq = generate_model(func, spatial_domain, spatial_resolution, force_location_id, density, tension, damping_ratio)
    
    # Setting up temporal domain
    sampling_freq = nyquist_freq * frequency_ratio
    num_samples = int(total_time * sampling_freq)
    temporal_domain = np.linspace(0, total_time, num_samples)
    print(f"Nyquist Freq (Suggested): {nyquist_freq:0.2f} [Hz] Chosen: {sampling_freq:0.2f} [Hz] which yielded {num_samples} samples")

    # Impact time processing
    impact_time_id = np.argmin(np.abs(temporal_domain - impact_time))

    # Forcing signal
    forcing = np.zeros_like(temporal_domain)
    forcing[impact_time_id] = 150  # Assuming a constant force value

    # Running the simulation
    t_out, _, x_out = simulate(system, forcing, temporal_domain)  # Assuming simulate is defined elsewhere
    x_out = x_out[:, :num_elements]

    return t_out, x_out, spatial_domain, temporal_domain
