import argparse
import simulation as sim
import matplotlib.pyplot as plt
import numpy as np

def finite(settings):
    # Finite System
    print("Simulating the finite system")
    num_force = 5
    force_locs = np.linspace(0.2, 0.8, num_force)
    energies = []
    states = []
    times = []

    for force_loc in force_locs:
        t,  x, spatial_domain, temporal_domain = sim.simulate_string(sim.string_fea_finite, force_loc, settings["spatial_properties"], settings["temporal_properties"], settings["material_properties"])
        e = np.cumsum(x ** 2, axis=0)
        energies.append(e)
        times.append(t)
        states.append(x)

    energies = np.array(energies)
    times = np.array(times)
    states = np.array(states)
    print(f"Energy: {energies.shape}, Time: {times.shape}, States: {states.shape}")
    np.savez_compressed('finite.npz', energies=energies, times=times, states=states, spatial_domain=spatial_domain, temporal_domain=temporal_domain)


def infinite(settings):
    # Infinite System
    # print("Simulating the infinite system")
    # t_infinite, x_infinite, spatial_domain_infinite, temporal_domain_infinite = sim.simulate_string(sim.string_fea_infinite, settings["spatial_properties_infinite"], settings["temporal_properties"], settings["material_properties"])
    # sim.prepare_figures((t_finite, t_infinite), (x_finite, x_infinite), vid_times, ()
    pass

if __name__ == '__main__':
    parser = argparse.ArgumentParser(description="Read a JSON file and process its contents.")
    parser.add_argument("file_path", type=str, help="Path to the JSON file")
    args = parser.parse_args()
    settings = sim.read_json_file(args.file_path)

    finite(settings)
    # infinite(settings)
