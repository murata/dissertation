
import matplotlib
matplotlib.use('Agg')  # Use a non-interactive backend such as Agg

import numpy as np
import scipy.integrate as integrate
import scipy.stats as stats
from sympy import symbols, log, series, lambdify
import matplotlib.pyplot as plt
import seaborn as sns


def calculate_e_true(mu_epsilon, snr_db):
    # Convert SNR from dB to linear scale
    snr_linear = 10 ** (snr_db / 10)

    # Calculate e_true
    e_true = snr_linear * mu_epsilon
    return e_true

# Define the function Delta_epsilon (error function)
def Delta_epsilon(epsilon, beta, e_true):
    return -(1/beta) * np.log(1 + epsilon/e_true)

# Define the PDF of epsilon
def pdf_epsilon(epsilon, mu_epsilon, sigma_epsilon):
    return stats.norm.pdf(epsilon, mu_epsilon, sigma_epsilon)

# Taylor expansion of the Delta_epsilon function
def taylor_expansion(order):
    epsilon = symbols('epsilon')
    function = -log(1 + epsilon/e_true) / beta
    taylor_series = series(function, epsilon, mu_epsilon, order+1).removeO()
    return lambdify(epsilon, taylor_series, 'numpy')

# Analytical Approach for mean using Taylor expansion
def analytical_mean_taylor(order):
    expanded_function = taylor_expansion(order)
    integrand = lambda epsilon: expanded_function(epsilon) * pdf_epsilon(epsilon)
    return integrate.quad(integrand, 0, np.inf)[0]

# LOTUS for Mean and Variance
def lotus_mean(beta, mu_epsilon, sigma_epsilon, e_true):
    integrand = lambda epsilon: Delta_epsilon(epsilon, beta, e_true) * pdf_epsilon(epsilon, mu_epsilon, sigma_epsilon)
    return integrate.quad(integrand, 0, np.inf)[0]

def ambarkutuk_mu_approx(beta, mu_epsilon, sigma_epsilon, e_true, num_terms=100):
    first_term = -1/beta * np.log(mu_epsilon / e_true + 1)
    
    def alpha_k(k):
        if k % 2 == 0:  # even k
            return np.prod(np.arange(1, k, 2)) * (sigma_epsilon**k)
        else:  # odd k
            return 0

    series_sum = sum([((-1)**k * alpha_k(k)) / (k * ((e_true + mu_epsilon)**k)) 
                      for k in range(1, num_terms + 1)])
    return first_term + (1/beta) * series_sum

def plot_results(differences, snr_values, sigma_epsilon_values, orders):
    sns.set(style="whitegrid")
    f, axes = plt.subplots(1, len(snr_values), figsize=(10, 3), sharex=True, sharey=True)

    for i, snr in enumerate(snr_values):
        ax = axes[i]
        for l, sigma_epsilon in enumerate(sigma_epsilon_values):
            sns.lineplot(x=orders, y=differences[i, :, l], ax=ax, 
                         label=f'$\sigma_\epsilon$: {sigma_epsilon:.2f}',
                         marker='o', markersize=8)

        ax.set_title(f'SNR: {int(snr):2} dB')
        ax.set_xlabel("Approximation Order")
        if i == 0:
            ax.set_ylabel("Error [%]")

    plt.tight_layout()
    return f

def magic(beta, mu_epsilon, sigma_epsilon_values, snr_values, orders):
    percent_error = np.zeros((len(snr_values), len(orders), len(sigma_epsilon_values)))  # For mean and variance

    for snr_idx, snr in enumerate(snr_values):
        e_true = calculate_e_true(mu_epsilon, snr)
        for sigma_idx, sigma_epsilon in enumerate(sigma_epsilon_values):
            mean_lotus = lotus_mean(beta, mu_epsilon, sigma_epsilon, e_true)
            for order_idx, order in enumerate(orders):
                mean_ambarkutuk = ambarkutuk_mu_approx(beta, mu_epsilon, sigma_epsilon, e_true, order)
                percent_error[snr_idx, order_idx, sigma_idx] = (mean_lotus - mean_ambarkutuk) / mean_lotus * 100

    return percent_error

if __name__ == '__main__':
    mu_epsilon = 15
    snr_values = [-5, 0, 5]  # Example range of SNR values
    sigma_epsilon_values = [1, 3, 5]  # Varying sigma_epsilon values
    orders = range(5)  # Orders of Taylor expansion to consider

    beta = -2.5
    percent_error = magic(beta, mu_epsilon, sigma_epsilon_values, snr_values, orders)
    fig = plot_results(percent_error, snr_values, sigma_epsilon_values, orders)
    fig.savefig("../proposal/figs/ambarkutuk_mean_estimation-1.eps", format="eps")
    print(percent_error)
