
import numpy as np
import scipy.integrate as integrate
import scipy.stats as stats
from sympy import symbols, log, series, lambdify


def calculate_e_true(mu_epsilon, snr_db):
    # Convert SNR from dB to linear scale
    snr_linear = 10 ** (snr_db / 10)

    # Calculate e_true
    e_true = mu_epsilon / snr_linear
    return e_true

# Define the function Delta_epsilon (error function)
def Delta_epsilon(epsilon):
    return -(1/beta) * np.log(1 + epsilon/e_true)

# Define the PDF of epsilon
def pdf_epsilon(epsilon):
    return stats.norm.pdf(epsilon, mu_epsilon, sigma_epsilon)

# Taylor expansion of the Delta_epsilon function
def taylor_expansion(order):
    epsilon = symbols('epsilon')
    function = -log(1 + epsilon/e_true) / beta
    taylor_series = series(function, epsilon, mu_epsilon, order+1).removeO()
    return lambdify(epsilon, taylor_series, 'numpy')

# Analytical Approach for mean using Taylor expansion
def analytical_mean_taylor(order):
    expanded_function = taylor_expansion(order)
    integrand = lambda epsilon: expanded_function(epsilon) * pdf_epsilon(epsilon)
    return integrate.quad(integrand, 0, np.inf)[0]

def analytical_variance_taylor(order):
    mean_taylor = analytical_mean_taylor(order)
    expanded_function = taylor_expansion(order)
    integrand = lambda epsilon: ((expanded_function(epsilon) - mean_taylor)**2) * pdf_epsilon(epsilon)
    return integrate.quad(integrand, 0, np.inf)[0]

# LOTUS for Mean and Variance
def lotus_mean():
    integrand = lambda epsilon: Delta_epsilon(epsilon) * pdf_epsilon(epsilon)
    return integrate.quad(integrand, 0, np.inf)[0]

def lotus_variance():
    mean = lotus_mean()
    integrand = lambda epsilon: (Delta_epsilon(epsilon) - mean)**2 * pdf_epsilon(epsilon)
    return integrate.quad(integrand, 0, np.inf)[0]

if __name__ == '__main__':
    # Given parameters
    beta = -0.2
    mu_epsilon = 5
    sigma_epsilon = mu_epsilon / 2
    snr = 10
    e_true = calculate_e_true(mu_epsilon, snr)
    # Calculate mean and variance using LOTUS
    mean_lotus = lotus_mean()
    variance_lotus = lotus_variance()
    print(f"Mean (LOTUS): {mean_lotus:0.4f} Variance (LOTUS): {variance_lotus:0.4f}")

    # # Calculate mean using Taylor expansion (n-th order)
    for order in range(10):
        mean_taylor = analytical_mean_taylor(order)
        variance_taylor = analytical_variance_taylor(order)
        print(f"Mean (Approximated):  {mean_taylor:0.4f} Variance (Approximated): {variance_taylor:0.4f} with {order:3}-th order approximation")
