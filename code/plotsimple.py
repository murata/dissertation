import numpy as np
import matplotlib.pyplot as plt
import scipy.stats as ss
import seaborn as sns 


def main():
    tfinal = 5
    fs = 500
    k = np.linspace(0, tfinal, tfinal * fs)
    z = 3 * np.cos(2*np.pi*k) * np.exp(-1*k)
    noise = np.random.randn(*z.shape) * 0.0625
    zeta = - 3 + noise
    z_hat = z + zeta
    fig, ax = plt.subplots(1, 1, figsize=(3, 1.5), dpi=100)
    ax.plot(k, z, 'r-.', label="$z[k]$")
    ax.plot(k, noise, 'k-', label="$z[k] + \zeta[k]$")
    ax.set_xlabel("Sample $k$")
    ax.set_ylabel("Amplitude [$V$]")
    ax.set_xticklabels([])
    ax.set_yticklabels([])
    fig.tight_layout()
    fig.savefig("../presentation-prelim/figures/signal-noise.png", dpi=100)

    fig, ax = plt.subplots(1, 1, figsize=(1.5, 1.5), dpi=100)
    ax.plot(z, zeta, 'r.')
    ax.set_xlabel("$z[k]$")
    ax.set_ylabel(r"$\nu[k]$ or $\zeta[k]$")
    ax.set_xticklabels([])
    ax.set_yticklabels([])
    ax.axis("equal")
    fig.tight_layout()
    fig.savefig("../presentation-prelim/figures/signal-noise-corr.png", dpi=100)

    fig, ax = plt.subplots(1, 1, figsize=(3, 1.5), dpi=100)
    ax.hist(zeta, 20, density=True)
    p = np.linspace(np.min(zeta), np.amax(zeta))
    q = ss.norm.pdf(p, np.mean(zeta), np.std(zeta))
    ax.plot(p, q, "r-.")
    ax.set_xlabel(r"$\nu[k]$ or $\zeta[k]$")
    ax.set_ylabel("Prob.")
    ax.set_xticklabels([])
    ax.set_yticklabels([])
    fig.tight_layout()
    fig.savefig("../presentation-prelim/figures/noise-histogram.png", dpi=100)

    fig, ax = plt.subplots(1, 1, figsize=(3, 1.5), dpi=100)
    ax.plot(k, z, 'r-.', label='$z[k]$')
    ax.plot(k, z_hat, 'k', label='$\hat{z}[k]$')
    ax.set_xlabel("Time $k$")
    ax.set_ylabel("Amp. $[V]$")
    ax.set_xticklabels([])
    ax.set_yticks([0])
    ax.set_yticklabels([0])
    ax.legend()
    fig.tight_layout()
    fig.savefig("../presentation-prelim/figures/ideal-nonideal-signal.png", dpi=100)

    fig, ax = plt.subplots(1, 1, figsize=(3, 1.5), dpi=100)
    ax.axvline(z.T @ z, color='r', linestyle="-.", label='$e$')
    ax.plot(ss.norm.pdf(k, 2.5, 0.25), color='k', linestyle="-", label='$\hat{e}$')
    ax.set_xlabel("Energy $[V^2]$")
    ax.set_ylabel("Probability")
    ax.set_xticklabels([])
    ax.set_yticks([0])
    ax.set_yticklabels([0])
    ax.legend()
    fig.tight_layout()
    fig.savefig("../presentation-prelim/figures/ideal-nonideal-energy.png", dpi=100)

def assumptions():
    e_start = 10
    beta = 2.5
    d = np.linspace(0, 3, 100)
    e = e_start * np.exp(-beta * d)
    fig, ax = plt.subplots(1, 1, figsize=(1.5, 1.5), dpi=100)
    ax.plot(d, e)
    ax.set_xlabel("Distance $d$ [$m$]")
    ax.set_ylabel("Energy $e$ [$V^2$]")
    ax.set_xticklabels([])
    ax.set_yticklabels([])
    fig.tight_layout()
    fig.savefig("../presentation-prelim/figures/energy.png", dpi=100)



if __name__ == '__main__':
    main()
    assumptions()
