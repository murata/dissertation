\begin{figure}[T]
    \centering
    \begin{tikzpicture}[x=1cm, y=1cm]
        \begin{scope}[shift={(0, 3)}]
            \draw[thick, dashed, color=skipper_smoke] (0, 0) rectangle(14,3);
            \node[align=center, anchor=center] at(7, 2.75) {\small Chapter 2: Uncertainty Quantification};
            \draw[dotted, color=gray] (0, 2.5) -- (14, 2.5);
            \draw<3->[smooth, domain=0.4:8.6, chicago_maroon] plot (\x, {0.7 + 0.9*exp(-(\x-1.8)^2*10)});
            \draw<3->[smooth, domain=0.4:8.6, chicago_maroon] plot (\x, {0.7 + 0.9*exp(-(\x-7.2)^2*10)});
            \draw[very thin, dashed] (0.4, 0.7) -- (0.4, 1.5) node[above] {\footnotesize$x=0$};
            \draw[very thin, dashed] (8.6, 0.7) -- (8.6, 1.5) node[above] {\footnotesize$x=L$};
            \draw[very thick] (0.4, 0.7) -- (8.6, 0.7);
            \fill (0.4, 0.7) -- (0.2, 0.4) -- (0.4, 0.4) node[below] {\footnotesize Fixed} -- (0.6, 0.4) -- cycle;
            \fill (8.6, 0.7) -- (8.4, 0.4) -- (8.6, 0.4) node[below] {\footnotesize Fixed} -- (8.8, 0.4) -- cycle;
            \fill[color=chicago_maroon] (4.25,0.7) rectangle (4.75, 1.2);
            \node at(4.5, 0.5) {\tiny Sensor $s_1$};
            \node<3->[align=center, anchor=center] at(4.5, 2) {\color{chicago_maroon}\small PDF $\f{{s_1}}$};
            \draw<3->[color=chicago_maroon, thin, -latex] (3.5, 2) -- (1.9, 1.5);
            \draw<3->[color=chicago_maroon, thin, -latex] (5.5, 2) -- (7, 1.5);
            \draw[latex-] (2, 0.7) -- (2, 1.2);
            \node at(2, 0.45) {\footnotesize $ u[k] $};
        \end{scope}
        \begin{scope}[shift={(9.6, 3)}]
            \draw<2->[-latex] (0,0.7) -- (3,0.7) node[below] {\tiny Distance $d$ [m]}; % x-axis
            \draw<2->[-latex] (0,0.7) -- (0,2.2) node[right] {\tiny Energy $e$ [V]}; % y-axis
            % Drawing the exponentially decaying curve
            \draw<2->[domain=0:3, smooth, variable=\x, chicago_maroon] plot ({\x}, {exp(-\x) + 0.7});
        \end{scope}
        \begin{scope}
            \draw[thick, dashed, color=skipper_smoke] (0, 0) rectangle(14,3);
            \node[align=center, anchor=center] at(7, 2.75) {\small Chapter 3: Sensor Fusion};
            \draw[dotted, color=gray] (0, 2.5) -- (14, 2.5);
            \draw<4->[smooth, domain=0.4:8.6, chicago_maroon] plot (\x, {0.7 + 0.9*exp(-(\x-1.8)^2*10)});
            \draw<4->[smooth, domain=0.4:8.6, chicago_maroon] plot (\x, {0.7 + 0.9*exp(-(\x-7.2)^2*10)});
            \draw<6->[smooth, domain=0.4:8.6, burnt_orange] plot (\x, {0.7 + 0.9*exp(-(\x-2.2)^2*2)});
            \draw<7->[smooth, very thick, domain=0.4:8.6, pylon_purple] plot (\x, {0.7 + 2*exp(-(\x-2)^2*30)});
            \node<7->[align=center, anchor=center] at(2, 2.25) {\color{pylon_purple}\small $\f{{s_1}} \otimes \f{{s_2}}$};


            \draw<4->[very thin, dashed] (0.4, 0.7) -- (0.4, 1.5) node[above] {\footnotesize$x=0$};
            \draw<4->[very thin, dashed] (8.6, 0.7) -- (8.6, 1.5) node[above] {\footnotesize$x=L$};
            \draw<4->[very thick] (0.4, 0.7) -- (8.6, 0.7);
            \fill<4-> (0.4, 0.7) -- (0.2, 0.4) -- (0.4, 0.4) node[below] {\footnotesize Fixed} -- (0.6, 0.4) -- cycle;
            \fill<4-> (8.6, 0.7) -- (8.4, 0.4) -- (8.6, 0.4) node[below] {\footnotesize Fixed} -- (8.8, 0.4) -- cycle;
            \fill<4->[color=chicago_maroon] (4.25,0.7) rectangle (4.75, 1.2);
            \node<4-> at(4.5, 0.5) {\tiny Sensor $s_1$};
            \fill<5->[color=burnt_orange] (7.25,0.7) rectangle (7.75, 1.2);
            \node<5-> at(7.5, 0.5) {\tiny Sensor $s_2$};
            \draw<5->[latex-] (2, 0.7) -- (2, 1.2);
            \node<6->[align=center, anchor=center] at(4.5, 2) {\color{burnt_orange}\small PDF $\f{{s_2}}$};
            \draw<6->[color=burnt_orange, thin, -latex] (3.5, 2) -- (3, 1.2);
            \node<4-> at(2, 0.45) {\footnotesize $ u[k] $};
        \end{scope}
    %     \begin{scope}[shift={(9.2, 0)}]
    %         \draw[color=white] (0, 0) rectangle (5, 2.2); 
    %     \end{scope}
    \end{tikzpicture}
\end{figure}
