\begin{figure}[bth!]
    \centering
    \begin{tikzpicture}[
        roundnode/.style={circle, draw=chicago_maroon, fill=chicago_maroon!25, very thick, radius=20mm},
        squarednode/.style={rectangle, draw=red!60, fill=red!15, very thick, minimum size=15mm},
        pylon_purple/.style={rectangle, draw=pylon_purple!60, fill=pylon_purple!15, very thick, minimum size=15mm},
        virginia_sunset/.style={rectangle, draw=virginia_sunset!60, fill=virginia_sunset!15, very thick, minimum size=15mm},
        target/.style={circle, draw=burnt_orange!60, fill=burnt_orange!5, thick, minimum size=3mm},
        pdfnode/.style={circle, draw=blue!60, fill=blue!5, very thick},
        others/.style={rectangle, draw=gray!60, fill=gray!15, very thick, minimum size=15mm}
        ]

        \begin{scope}
            \draw [step=0.15cm, black!10, thin] (0, 0) grid(13.65, 5);
            % \begin{scope}[opacity=.1,transparency group=knockout]
            %     \shade[shading=radial] (0,0) rectangle(13.65, 5);
            % \end{scope}
            % \begin{scope}[transparency group,opacity=0.1]
            %     \shade[inner color=red, fill=white, fill opacity=0.5] (7, 2.5) circle [radius=1.5cm];
            %   \end{scope}
            \draw [black!75, thin] (0, 0) rectangle(13.65, 5);
            % \node at (7, 2) [align=center]{\footnotesize  $\arrow$ \\ \footnotesize Occupant Loc.};
            \begin{scope}[xshift=6cm, yshift=1cm]
                \draw [-latex, black, very thick] (0, 0) -- ++(1.5, 0);
                \draw [-latex, black, very thick] (0, 0) -- ++(0, 1.5);
                \node at (-0.15, 1.5) {\footnotesize $y$};
                \node at (1.5, -0.25) {\footnotesize $x$};
            \end{scope}
            \shade[ball color=green] (7, 2.5) circle [radius=0.125cm];
            \draw [-latex, thick, green] (6, 1) -- (7, 2.5);
            \node at (7, 1.75) {\footnotesize \color{green} $\vect{x}_{true}$};

            \begin{scope}[xshift=10.65cm, yshift=1.5cm]
                \draw [draw=red, fill=red!10, dashed, thin] (-0.75, -0.75) rectangle(1.75, 1.75);
                \draw [draw=red, dashed, very thin] (-0.75, -0.25)  -- ++(2.5, 0);
                \node at (0.5, -0.5) {\footnotesize Sensor $j$};

                \draw [-latex, red, thick] (0, 0) -- ++(1.5, 0);
                \draw [-latex, red, thick] (0, 0) -- ++(0, 1.5);
                \draw [red, ->] (0.2, 0) arc (0:142:0.2);
                \node at (0.25, 0.25) {\footnotesize \color{red} $\theta_j$};
            \end{scope}
            % \shade[ball color=red] (7.5, 3.25) circle [radius=0.125cm];
            \draw [-latex, thick, red] (10.65, 1.5) -- (7.5, 3.25);
            \node at (8.8, 2.8) [rotate=-30] {\footnotesize \color{red} $d_j = g(e_j; \vect{\beta}_j)$};
            \draw [-latex, thick, red, dashed] (7.5, 3.25) -- (7, 2.5);
            \node at (7.5, 2.75) {\footnotesize \color{red} $\vect{\chi}_{j}$};

            \draw [-latex, red!80, thick, dotted] (6, 1) -- (10.65, 1.5);
            \node at (9, 1) {\footnotesize \color{red} $\vect{t}_j$};

            \begin{scope}[xshift=2cm, yshift=1.5cm]
                \draw [draw=blue, fill=blue!10, dashed, thin] (-0.75, -0.75) rectangle(1.75, 1.75);
                \draw [draw=blue, dashed, very thin] (-0.75, -0.25)  -- ++(2.5, 0);
                \node at (0.5, -0.5) {\footnotesize Sensor $i$};

                \draw [-latex, blue, thick] (0, 0) -- ++(1.5, 0);
                \draw [-latex, blue, thick] (0, 0) -- ++(0, 1.5);

                \draw [blue, ->] (0.5, 0) arc (0:50:0.5);
                \node at (0.65, 0.16) {\footnotesize \color{blue} $\theta_i$};
            \end{scope}
            % \shade[ball color=blue] (4.5, 4.5) circle [radius=0.125cm];
            \draw [-latex, thick, blue] (2, 1.5) -- (4.5, 4.5);
            \node at (3.65, 4) [rotate=50] {\footnotesize \color{blue} $d_i = g(e_i; \vect{\beta}_i)$};
            \draw [-latex, thick, blue, dashed] (4.5, 4.5) -- (7, 2.5);
            \node at (6, 3.5) {\footnotesize \color{blue} $\vect{\chi}_{i}$};
            
            \draw [-latex, blue!80, thick, dotted] (6, 1) -- (2, 1.5);
            \node at (4.5, 1) {\small \color{blue} $\vect{t}_i$};

            \node at (12.25, 4.65)[align=center] {\small $\uparrow$ \\ \small Floor Boundaries $\rightarrow$};
        \end{scope}
    \end{tikzpicture}
    \caption{This figure visualizes some key variables frequently used in the paper. The blue and red box represent sensor $i$ and sensor $j$ which reside at $\vect{t}_i$ and $\vect{t}_j$, respectively.
    When an occupant excites the floor with their footstep which is occured at $\vect{x}_{true}$, $m$ accelerometers first estimate $\forall d_i \in \{1, \ldots, m\}$.
    Therefore, the estimated location vector of the occupant location by sensor $i$ can be seen as the vector summation of its location vector $\vect{t}_i$ and the estimated $d_i$ for some $\theta_i$.}
    \label{fig:multi-sensor-layout}
\end{figure}

