\section[Chapter 2]{Chapter 2: UQ of Energy-based Vibro-localization}
\begin{frame}[t, squeeze]{Problem Definition -- \rnum{1}}{Graphical Representation \& Summary}
    \input{../proposal/figs/single-sensor.tikz}
    \begin{textblock*}{\textwidth}(10mm,20mm)
        \begin{alertblock}<only@2>{Uncertainty Quantification Problem}
            Measured:
            \begin{itemize}
                \item Floor vibration due to the occupants footfall patterns
            \end{itemize}
            \emph{A priori}:
            \begin{itemize}
                \item True measurement vector $\vect{z}$
                \item Parameters of the showing how vibration wavefronts propagate along the floor
                \item Statistical characteristics of the measurement error
                \item Occupant location $\vect{x}$
                \item A localization function
            \end{itemize}
            \vspace*{10pt}
            \hrule
            \vspace*{10pt}
            \centering
            \textbf{Goal:} \alert{Characterize the localization error $\vect{\chi}$}
            \vspace*{10pt}
        \end{alertblock}
    \end{textblock*} 
\end{frame}

\begin{frame}[t]{Probem Definition -- \rnum{2}}
    \vspace{-0.5cm}
    \begingroup
        \small
        % \centering
        \begin{columns}[T]
            \begin{column}{0.48\textwidth}
                \begin{definition}
                    \alert{True vibration} $z[k] \in \mathbb{R}$ is the time-domain measurements of the sensor for $k \in \{1, \ldots, \text{\gls{nk}}\}$.
                    \[ 
                        \boxed{\vect{z} = (z[1], \ldots, z[n_k])^\top \in \mathbb{R}^{n_k}}
                    \]
                \end{definition}
                \begin{definition}
                    \alert{Measurement vector} \( \vect{\hat{z}} \) is the sum of \alert{true vibration} \( \vect{z} \), a constant bias \( \delta \in \mathbb{R} \) and a random measurement error vector \( \vect{\nu} \).
                    \[
                        \boxed{\vect{\hat{z}} = \vect{z} + \vect{\nu} + \delta}
                    \]
                \end{definition}
            \end{column}
            \begin{column}{0.48\textwidth}
                \includegraphics[scale=0.90]{figures/ideal-nonideal-signal.png}
                \includegraphics[scale=0.90]{figures/ideal-nonideal-energy.png}
            \end{column}
        \end{columns}
    \endgroup
\end{frame}

\begin{frame}[t]{Assumptions}
    \vspace{-0.5cm}
    \begin{columns}[T]
        \begin{column}{0.48\textwidth}
            \justifying
            {
                \small
                \underline{Distribution of errors:} Errors $\nu[k]$ are \emph{iid} samples of a \alert{normal random variable} with known varience.
                \[
                    \vect{\nu} = ( \nu[1], \ldots, \nu[n_k] )^\top \in \mathbb{R}^{n_k}, \ \nu[k] \sim \N{0}{\sigma_\nu}
                \]
                \underline{Energy-based Localization:} Adapted from \cite{alajlouni2020passive}.
                
                \[ e = e_0 \, \exp{\left( -d \beta \right)} \]
                \underline{Indepedency:} The \alert{signal} $z[i]$ and the \alert{disturbance} $\zeta[k]$ are \alert{indepedent} of each other.
                \[
                    \rho_{z, \zeta} = \corr{z[i], \zeta[j]} = 0, \ \forall i,j \in \{1, \ldots, n_k \}
                \]
                \underline{Assumed Radial Localization:} $\theta - \hat{\theta} = 0$.
            }
        \end{column}
        \begin{column}{0.48\textwidth}
            \begingroup
            \footnotesize
                % \begin{figure}[t]
                \centering
                \includegraphics[scale=0.9]{figures/noise-histogram.png}
                \includegraphics[scale=0.85]{figures/energy.png}
                \includegraphics[scale=0.85]{figures/signal-noise-corr.png}
                % \end{figure}
            \endgroup
        \end{column}
    \end{columns}
\end{frame}

\begin{frame}[t]{Derivation -- \rnum{1}}{\gls{pdf} of Error in the Signal Energy $\f{\varepsilon}$}
    \vspace{-0.5cm}
    \begin{columns}[T]
        \begin{column}{0.48\textwidth}
            \begin{itemize}
                \item The estimation error is given by $\vect{\chi} = \vect{x} - \vect{\hat{x}} \in \mathbb{R}^2$.
                \item It is governed by the error in the measured signal energy $\varepsilon = e - \hat{e}$.
            \end{itemize}
            \[
                \norm{\vect{\chi}} = \Delta\left( \varepsilon \right) = \norm{\vect{x} - \vect{\hat{x}}} = -\frac{1}{\beta} \log{\left( 1 + \frac{\varepsilon}{e} \right)}\in \mathbb{R}
            \]
            where $e = \vect{z}^\top \vect{z} \in \mathbb{R}_+$, $\hat{e} = \hat{\vect{z}}^\top \hat{\vect{z}} \in \mathbb{R}_+$.
            In the dissertation proposal, we showed that:
            \begin{equation}
                \boxed{
                    \varepsilon \sim \f{\varepsilon} = \N{\mu_\varepsilon}{\sigma_\varepsilon}
                }~.
            \end{equation}
        \end{column}
        \begin{column}{0.48\textwidth}
            \begin{figure}[t]
                \centering
                \includegraphics[width=144pt, height=144pt]{figures/lotus-joint.png}
            \end{figure}
        \end{column}
    \end{columns}
\end{frame}

\begin{frame}[t]{Derivation -- \rnum{2}}{Law of Unconscious Statistician (LOTUS)}
    \vspace{-0.5cm}
    \begin{columns}[T]
        \begin{column}{0.65\textwidth}
            \begingroup
            \footnotesize
                The mean of the localization error is obtained by employing LOTUS.
                Hence, we can state the mean as given by,
                \begin{equation}
                    \boxed{
                        \mu_{\norm{\vect{\chi}}} = \expt{\Delta(\varepsilon)} = \int \underbrace{\Delta(\varepsilon)}_{-\frac{1}{\beta}\log{\left(1 + \frac{\varepsilon}{e}\right)}} \, \f{\varepsilon} \,\diff{\varepsilon}~.
                    }
                \end{equation}
                Simplify $\Delta(\varepsilon)$ by employing Taylor expansion around $\frac{\mu_\varepsilon}{e}$: 
                \[
                    \mu_{\norm{\vect{\chi}}} = \int \Delta(\varepsilon) \f{\varepsilon} \, \diff \varepsilon  \approx \int \hat{\Delta}(\varepsilon) \f{\varepsilon} \, \diff \varepsilon
                \]
            \endgroup
        \end{column}
        \begin{column}{0.34\textwidth}
            \begingroup
                \footnotesize
                \begin{alertblock}{Reminder: LOTUS}
                    The mean of a random variable $y = g(x)$, where $x \sim \f{x}$ is given by:
                    \[
                        \mu_y = \int g(x) \f{x} \,\diff x~.
                    \]
                \end{alertblock}
            \endgroup
        \end{column}
    \end{columns}
    \begingroup
        \footnotesize
        The proposal contains a detailed derivation of the approximate mean localization error. 
        \begin{equation}
            \boxed{
                \hat{\mu}_{\norm{\vect{\chi}}} = -\frac{1}{\beta} \log{\left(\frac{\mu_\varepsilon}{e} + 1\right)} + \frac{1}{2\beta} \sum_{k=1}^{\infty} \frac{ (2k-1)!! \sigma_\varepsilon^{2k}}{k \left(e + \mu_\varepsilon\right)^{2k} \, e^{2k}}
            }~.
            \label{eq:lotus-mean}
        \end{equation}
    \endgroup
\end{frame}

\begin{frame}[t]{Study Details \& Results}
    \vspace{-0.5cm}
    \begin{itemize}
        \item \texttt{Mean of Energy Error}: \( \mu_\varepsilon =  15 \si{\volt^2} \) \\
        \item \texttt{Std. Dev. of Energy Error:} \(\sigma_\varepsilon \in \{1, 3, 5\} \si{\volt^2}\) \\
        \item \texttt{True Energy}: \( e \in \{4.74, 15, 47.43 \} \si{\volt}\) (corresponding SNR \( \{-5, 0, 5\} \si{\decibel} \)) \\
        \item \texttt{Approximation Order:} \( n \in \{ 0, 1, 2, 3, 4 \} \)
    \end{itemize}
    \hrulefill
    \begin{figure}
        \centering
        \includegraphics[width=\textwidth]{figures/ambarkutuk-imac-study.png}
    \end{figure}
\end{frame}

\begin{frame}[t, squeeze]{Summary of Contributions}
    We reported to the community that sensor-based uncertainty sources can be model in terms of signal envelope and bias.
    \begin{itemize}
        \item Localization error $\vect{\chi}$ is \alert{not} normally distributed
        \item Mean localization error is \alert{not} negligibly small.
        \item Linearized models \alert{approximate} the actual \glspl{pdf} under \emph{specific} circumstances regardless of SNR values observed in the sensing system.
    \end{itemize}
    \hrulefill
    \\
    Now that we have a complete sensor model, any error have not been acknowledged should be due to the floor-dependent errors. 
    \begin{itemize}
        \item Dispersion
        \item Non-uniform material characteristics
        \item Boundary conditions (reflections)
    \end{itemize}
\end{frame}

