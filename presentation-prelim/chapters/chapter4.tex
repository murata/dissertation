\section[Chapter 4]{Chapter 4: Vibro-Localization of Multiple Footsteps with Multiple Sensors}
\begin{frame}[t]{Introduction}
    \vspace{-0.5cm}
    \input{figures/layout-sd.tikz}
    \begin{textblock*}{\textwidth}(10mm,15mm)
        \begin{alertblock}<only@2>{Multi-Sensor Localization Problem}
            Measured:
            \begin{itemize}
                \item Floor vibration measurements of \alert{many} sensors.
            \end{itemize}
            \emph{A priori}:
            \begin{itemize}
                \item \st{True measurement vector $\vect{z}$}
                \item \st{Occupant's distance $d$}
                \item \st{Occupant's radial distance $\theta$ }
                \item Parameters of the showing how vibration wavefronts propagate along the floor
                \item Statistical characteristics of the measurement error
            \end{itemize}
            \vspace*{10pt}
            \hrule
            \vspace*{10pt}
            \centering
            \textbf{Goal:} \alert{Estimate a location vector $\hat{\vect{x}}$ by minimizing some aspect of the error $\vect{\chi}$.}
            \vspace*{10pt}
        \end{alertblock}
    \end{textblock*} 
\end{frame}

\begin{frame}[t]{Coctail Party Problem}
    \vspace{-0.5cm}
    \begin{columns}[T]
        \begin{column}{0.48\textwidth}
            \underline{Coctail Party Problem~\cite{Cherry1953}:}
            \small
            \begin{itemize}
                \item A phenomenon in auditory processing where an individual focuses on a single auditory source in a noisy environment.
                \item The individual \alert{isolates} one conversation amidst multiple competing sounds, like in a cocktail party.
                \item This problem illustrates the complexity of \alert{selective attention} and the human auditory system's capacity to \alert{segregate and process} specific acoustic signals from a mixture.
            \end{itemize}
        \end{column}
        \begin{column}{0.48\textwidth}
            \begin{figure}[t]
                \centering
                \includegraphics[width=.80\textwidth]{figures/coctail-party.png}
                \caption{\footnotesize Obtained from a generative AI model}
            \end{figure}
        \end{column}
    \end{columns}
\end{frame}

% \begin{frame}[t, label=ch4]{Established Solutions}
%     \begin{itemize}
%         \item EMD
%         \item ICA
%         \item PCA
%     \end{itemize}
% \end{frame}

\begin{frame}[t]{Methodology}
    \begin{columns}[T]
        \begin{column}{0.48\textwidth}
            \begin{definition}[Decomposition of Signals]
                \small
                Signal decomposition is the process of breaking down a signal into its constituent components
                Representing the space of original signals as \( S \), a decomposition operation \( \mathcal{D} \) can be defined as a mapping:
            
                \[ \mathcal{D}: S \rightarrow \{C_0, \ldots, C_{n-1}\}~, \]
            
                where each \( C_i \) is a component of the original signal \( s \in S \).
            \end{definition}
        \end{column}
        \begin{column}{0.48\textwidth}
            \begin{definition}[Reconstruction of Signals]
                \small
                Signal reconstruction is the process of reassembling the components of a decomposed signal back into its original form.
                \[
                    \mathcal{D}^{-1}: \{C_0, \ldots, C_{n-1}\} \rightarrow S
                \]
                where a set of components \( \{C_0, \ldots, C_{n-1}\} \) is transformed back into the original signal \( s \in S \).
            \end{definition}
        \end{column}
    \end{columns}
\end{frame}

\begin{frame}[t]{Methodology}{Sigmoid Decomposition}
        \begin{itemize}
            \item Let \( \vect{x} = \left( x[1], \ldots,  x[n] \right) \) be an arbitrary signal that contains measurements of an event.
            \item We propose a (power-centric) decomposition, \gls{sd} \( \mathcal{S} \), that dissects the signal into a pyramid-like structures:
            \[
                \boxed{
                    \mathcal{S}: \vect{x} \rightarrow \{ \mathcal{W}, \mathcal{B}, \vect{m} \}
                }~.
            \]
            where $\mathcal{W}$ and $\mathcal{B}$ are collection of vectors of different size and $\vect{m}$ is a vector same size as $\vect{x}$.
            \item The reconstruction of the signal from its components are called inverse-\gls{sd}.
            \[
                \boxed{
                    \mathcal{S}^{-1}: \{ \mathcal{W}, \mathcal{B}, \vect{m} \} \rightarrow \vect{x}
                }~.
            \]
        \end{itemize}
\end{frame}

\begin{frame}[c, plain, squeeze]
    \begin{columns}[T]
        \begin{column}{0.65\textwidth}
            \begin{algorithm}[H]
                \begin{algorithmic}[1]
                \footnotesize
                \Require Signal $x[n]$, $n \in \mathcal{N} = \{0, \ldots, N-1\}$
                \Ensure Feature Pyramids $\mathcal{W}, \mathcal{B}$ and Masking Vector $\vect{m}$
        
                \Function{SigmoidDecomposition}{$x[n]$}
                    \State Compute the cumulative power of $x[n]$: $p[n] \gets \sum_{i=0}^{N-1} \lvert x[i] \rvert^2$
                    \State Initialize feature pyramids: $\mathcal{W} \gets \{\}, \mathcal{B} \gets \{\}$
                    \For{$k \gets 0$ \textbf{to} $N-1$}
                        \State Let $\Delta = \frac{N}{k+1}$
                        \State Initialize weights and biases: $\vect{w}_k \gets \vect{0}_{k+1}, \vect{b}_k \gets \vect{0}_{k+1}$
                        \For{$i \gets 0$ \textbf{to} $k$}
                            \State $\vect{w}_k[i] \gets p[\lfloor (i+1) \Delta \rfloor] - p[\lfloor i \Delta \rfloor]$
                            \State $\vect{b}_k[i] \gets \lfloor \Delta (i + \frac{1}{2})\rfloor$
                            \If{$\vect{w}_k[i] > 0$}
                                \State $\vect{b}_k[i] \gets \argmax_{n \in \{\lfloor i \Delta \rfloor, \ldots, \lfloor (i+1) \Delta \rfloor \}}{\frac{\diff}{\diff n}p[n]}$
                            \EndIf
                        \EndFor
                        \State $\mathcal{W} \gets \mathcal{W} \cup \{\vect{w}_k\}$
                        \State $\mathcal{B} \gets \mathcal{B} \cup \{\vect{b}_k\}$
                    \EndFor
                    \State Compute masking vector: $\vect{m} \gets \sign{(x[n])}$
                    \State \Return $(\mathcal{W}, \mathcal{B}, \vect{m})$
                \EndFunction
                \end{algorithmic}
            \end{algorithm}
        \end{column}
        \begin{column}{0.34\textwidth}
            \includegraphics[width=0.8\textwidth]{figures/demosignals.png}
            \includegraphics[width=0.8\textwidth]{figures/demopowers.png}
            % \includegraphics[width=0.8\textwidth]{example-image-a}
        \end{column}
    \end{columns}
\end{frame}

\begin{frame}{\gls{sd} Algorithm}{Feature Pyramids}
    \input{figures/pyramid.tikz}
\end{frame}

\begin{frame}{Signal Reconstruction}
    The inverse transformation, denoted as \( \mathcal{S}^{-1} \), aims to reverse the effects of the \gls{sd}, combining the components represented in the feature pyramids to approximate the original signal.

    \[
        \mathcal{S}^{-1}: \{ \mathcal{W}, \mathcal{B}, \vect{m} \} \rightarrow \vect{x}
    \]

    The reconstruction process from a single layer $k$ can be expressed as follows:
    \[
        \hat{p}[n] = \sum_{i=0}^{k-1} \vect{w}_k[i] \cdot \sigma \left[ \frac{n - \vect{b}_k[i]}{\kappa} \right]
    \]

    Once the cumulative power series is constructed, the original signal \( x[n] \) can be obtained as:
    \[
        \hat{x}[n] = \sqrt{ \hat{p}[n] - \hat{p}[n-1] } \cdot m[n]
    \]
\end{frame}

\begin{frame}[t]{Preliminary Results -- \rnum{1}}{Decomposition and Reconstruction}
    \begin{figure}[ht]
        \includegraphics[width=0.95\textwidth]{figures/signals.png}
        \centering
        \caption{The simulated signal and its reconstruction after employing the \gls{sd} on it with varying smoothing parameter $\kappa$ and layer depth.}
    \end{figure}
\end{frame}
\begin{frame}[t]{Preliminary Results -- \rnum{1}}{Decomposition and Reconstruction}
    \begin{figure}[ht]
        \centering
        \includegraphics[width=0.95\textwidth]{figures/powers.png}
        \caption{The cumulative power of the simulated signal and its reconstruction after employing the \gls{sd} on it with varying smoothing parameter $\kappa$ and layer depth.}
    \end{figure}
\end{frame}

\begin{frame}[label=current]{Summary of Anticipated Contributions}
    \begin{itemize}
        \normalsize
        \item Modeling selective attention mechanisms by employing \gls{sd}
        \item Robust event detection
        \item Multi-sensor signal separation based on salient features of \gls{sd}
    \end{itemize}
\end{frame}
