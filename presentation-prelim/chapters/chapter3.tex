\section[Chapter 3]{Chapter 3: Vibro-Localization of a Footstep with a Multiple Sensors}
\begin{frame}[t]{Multi-sensor Localization}
    \vspace{-0.5cm}
    \input{../proposal/figs/layout.tikz}
    \begin{textblock*}{\textwidth}(10mm,15mm)
        \begin{alertblock}<only@2>{Multi-Sensor Localization Problem}
            Measured:
            \begin{itemize}
                \item Floor vibration measurements of \alert{many} sensors.
            \end{itemize}
            \emph{A priori}:
            \begin{itemize}
                \item \st{True measurement vector $\vect{z}$}
                \item \st{Occupant's distance $d$}
                \item \st{Occupant's radial distance $\theta$ }
                \item Parameters of the showing how vibration wavefronts propagate along the floor
                \item Statistical characteristics of the measurement error
            \end{itemize}
            \vspace*{10pt}
            \hrule
            \vspace*{10pt}
            \centering
            \textbf{Goal:} \alert{Estimate a location vector $\hat{\vect{x}}$ by minimizing some aspect of the error $\vect{\chi}$.}
            \vspace*{12pt}
        \end{alertblock}
    \end{textblock*} 
\end{frame}

% \begin{frame}[t squeeze, label=current]{}
%     \begin{table}[htb]
%         \centering
%         \footnotesize
%         \begin{tabularx}{\linewidth}{l CC}
%             \toprule
%             & \textbf{Baseline Technique}~\cite{alajlouni2019new} & \textbf{Proposed Technique} \\
%             \midrule
%             \shortstack{\textbf{Localization Feature} \\ (Measured)} &
%             \shortstack{Energy Measurements \\ \( e_1, \ldots, e_m \)} &
%             \shortstack{Energy Measurements \\ \( e_1, \ldots, e_m \)} \\
%             \midrule
%             \shortstack{\textbf{Known Parameters} \\ (\emph{A priori})} &
%             \shortstack{Sensor locations \\ \( \vect{t}_1, \ldots, \vect{t}_m \)} &
%             \shortstack{Sensor locations \\ \( \vect{t}_1, \ldots, \vect{t}_m \)} \\
%             \midrule
%             \shortstack{\textbf{Calibrated Parameters} \\ (Offline Processing)} &
%             \shortstack{None \\ \phantom{Asdas}} &
%             \shortstack{Sensor noise profile: $\mu_\zeta, \sigma_\zeta$ \\ Calibration vectors $\vect{\beta}_1, \ldots, \vect{\beta}_m$} \\ % Corrected this line
%             \midrule
%             \shortstack{\textbf{Output} \\ (Online Processing)} &
%             \shortstack{Location estimate: $\hat{\vect{x}}$ \\ \phantom{SDa}} &
%             \shortstack{Location estimate: $\vect{x}^*$ \\ Consensus set: $\mathcal{C}$ and \\ its distribution: $f_{\mathcal{C}} \left(\vect{x}\right)$} \\
%             \bottomrule
%         \end{tabularx}
%         \caption{Comparative Overview of Baseline and Proposed Vibro-localization Techniques. This table illustrates the key differences in localization features, known and calibrated parameters, and output between the Baseline Technique as per~\citet{alajlouni2019new} and the proposed technique~\cite{s23239309}.}
%         \label{tab:comparison_techniques}
%     \end{table}
% \end{frame}

\begin{frame}[t]{}
    \begingroup
        \small
        \underline{Localization function:} The same localization function is restructured with each sensor's location vector
        \[
            \boxed{
                \vect{x}_i = \vect{h}(e_i) = \vect{t}_i + \underbrace{d_i}_{\triangleq g(e_i; \vect{\beta}_i)} \begin{bmatrix} \cos{\theta_i} \\ \sin{\theta_i} \end{bmatrix}
            }
        \]
        \underline{PDF of Localization function\footnote[frame]{Please see the proposed dissertation manuscript for the full derivation.}:} Employ the \gls{pdf} transformation theorem to obtain the \gls{pdf} of the estimated location vector $\vect{\hat{x}}_i$. 
        \[
            \boxed{
                f_{\vect{X}_i}\left(\hat{\vect{x}}_i; e_i, \vect{\beta}_i \right) = \frac{\left\lvert \frac{\partial g_{i}^{-1}}{\partial \hat{\vect{x}}_i}\left( \norm{\hat{\vect{x}}_i - \vect{t}_i}; \vect{\beta}_i \right) \right\rvert}{ \norm{\hat{\vect{x}}_i - \vect{t}_i} \sigma_E ({2\pi})^{\frac{3}{2}}}\exp{\left[-\frac{\left(g_{i}^{-1}\left(\norm{\hat{\vect{x}}_i-\vect{t}_i} \right)-\mu_E\right)^2}{2\sigma^2_E} \right]}
            }
        \]

        \underline{Sensor Fusion:} Combine measurements of many sensors by multipliying their \glspl{pdf}.
        \[
            \boxed{
                f_{\vect{X}_1, \ldots, \vect{X}_m}\left(\hat{\vect{x}}_1, \ldots, \hat{\vect{x}}_m; e_1, \ldots, e_m, \vect{\beta}_1, \ldots, \vect{\beta}_m \right) = \prod_{i=1}^{m} f_{\vect{X}_i}\left(\vect{x}_i; e_i, \vect{\beta}_i \right)
            }
        \]
    \endgroup
\end{frame}

\begin{frame}[t]{Byzantine Generals Problem}
    \begin{columns}[T]
        \begin{column}{0.48\textwidth}
            \underline{Byzantine Generals Problem~\cite{lamport1982}:}
            \justifying
            \begin{itemize}
                \item Several Byzantine army divisions, each led by a general, are camped outside an enemy city.
                \item They \alert{communicate} only by messenger and \alert{must agree} on a battle plan.
                \item Some generals might be \alert{traitors}, aiming to \alert{disrupt the concensus}.
            \end{itemize}
        \end{column}
        \begin{column}{0.48\textwidth}
            \begin{figure}[t]
                \centering
                \includegraphics[width=.80\textwidth]{figures/byzantine-generals.png}
                \caption{\footnotesize Obtained from a generative AI model}
            \end{figure}
        \end{column}
    \end{columns}
\end{frame}

\begin{frame}[t]{Byzantine Sensor Elimination}
    \input{figures/sensor_fusion_small.tikz}
\end{frame}

\begin{frame}[t, squeeze]{A Quick Look Into the Literature}
    \begin{table}[htb]
        \centering
        \small
        \begin{tabularx}{\linewidth}{lXX}
            \toprule
            & RANSAC & \citet{mirshekari2018occupant} \\
            \midrule
            \textbf{Primary Use} & 
            Estimating parameters of mathematical models in the presence of outliers, predominantly in computer vision. & 
            Elimination of far-away sensors in an adaptive multilateration technique of a vibro-localization system. \\
            \midrule
            \textbf{Methodology} & 
            Works by randomly selecting subsets of data and identifying the model with the highest consensus. &
            Identifying distant sensors in \gls{tdoa} estimations to avoid bias in multilateration algorithm. \\
            \midrule
            \textbf{Input Type} & 
            Points & 
            Time-domain measurements \\
            \bottomrule
        \end{tabularx}
        \caption{Comparison between RANSAC and \cite{mirshekari2018occupant}}
        \label{tab:comparison}
    \end{table}
\end{frame}

\begin{frame}[t, squeeze]{Information Theoretic \gls{bse}}{Summary}
    \begin{columns}[T]
        \begin{column}{0.70\textwidth}
            \begingroup
                \small
                % \begin{enumerate}
                %     \item
                    \underline{Initialization:} An initial consensus set $\mathcal{C}$ is found with a greedy approach.
                    \[
                        \boxed{
                            \mathcal{C} = \left\{i,j \mid \argmax_{i, j} \underbrace{\expt{-\log{f_{\vect{x}_i, \vect{x}_j}}\left( \vect{x} \right)}}_{\triangleq h_{i, j}} \right\}
                        }
                    \]
                    % \item
                    \underline{Expansion:} The consensus set $\mathcal{C}$ is expanded by sequentially adding new sensors into the set.
                    \[
                        \boxed{
                            \mathcal{C} = \mathcal{C} \cup 
                            \begin{cases}
                                \{i\}, & \text{if } h_{\mathcal{C}, i} > h_{\mathcal{C}} \\
                                \emptyset, & \text{otherwise}
                            \end{cases}, \forall i \in \mathcal{M}
                        }
                    \]
                % \end{enumerate}
            \endgroup
        \end{column}
        \begin{column}{0.29\textwidth}
            \begingroup
                \footnotesize
                \begin{alertblock}{Entropy}
                    Entropy measures the uncertainty (randomness, or, suprisal) of a random variable $X$ with a density function $f_X(x)$.
                    \[ 
                        h_X = -\int f_X(x) \log f_X(x) \, dx
                    \]
                \end{alertblock}
                \begin{alertblock}{Notice}
                    Complexity: $\mathcal{O}(m^3)$
                \end{alertblock}
            \endgroup
        \end{column}
    \end{columns}
\end{frame}

\begin{frame}[t, plain]{Localization with \gls{bse} Algorithm}
    \begin{columns}[T]
        \begin{column}{0.48\textwidth}
            \begingroup
                \begin{algorithm}[H]
                    \footnotesize
                    \begin{algorithmic}[1]
                        \Procedure{\texttt{BSE}}{$f, \mathcal{M}, \mathcal{S}$}
                            \State $h^* \gets -\infty$
                            \ForAll{$(i, j) \in \mathcal{M} \times \mathcal{M}$, $i \neq j$}
                                \State $h_{i,j} \gets \texttt{ENTROPY}(\texttt{FUSE}(f_{\vect{x}_i}, f_{\vect{x}_j}), \mathcal{S})$
                                \If{$h_{i,j} \leq h^*$} \State $h^*, \mathcal{C} \gets h_{i,j}, \{i, j\}$ \EndIf
                            \EndFor
                            \ForAll{$i \in \mathcal{M} \setminus \mathcal{C}$}
                                \State $h_{i} \gets \texttt{ENTROPY}(\texttt{FUSE}(f_{\mathcal{C}}, f_{\vect{x}_i}), \mathcal{S})$
                                \If{$h_{i} \geq h^*$}
                                    \State $h^* \gets h_{i}$
                                    \State $\mathcal{C} \gets \mathcal{C} \cup \{i\}$ \EndIf
                            \EndFor
                            \State \textbf{return} $\mathcal{C}$
                        \EndProcedure
                    \end{algorithmic}
                \end{algorithm}
            \endgroup
        \end{column}
        \begin{column}{0.48\textwidth}
            \begingroup
            \begin{algorithm}[H]
                \footnotesize
                \begin{algorithmic}[1]
                    \Procedure{\texttt{Localize}}{$\hat{e}_1, \ldots, \hat{e}_m$}
                        \State $\vect{\kappa} \gets \vect{1}_{m \times 1}$
                        \While{$\norm{\nabla \vect{\kappa}} \leq thr$}
                            \State $\vect{\kappa} = \vect{\kappa} + \nabla \vect{\kappa}$
                            \ForAll{$i \in \mathcal{M}$}
                                \State  $f_{\vect{x}_i} \gets \texttt{DENSITY}(\hat{e}_i \times \kappa_i)$
                            \EndFor
                            \State $\mathcal{C} \gets \texttt{BSE}(f_{\vect{x}_1}, \ldots, f_{\vect{x}_m}, \mathcal{M}, \mathcal{S})$
                            \State $f_{\mathcal{C}} \gets \texttt{FUSE}(f_{\vect{x}_i}~\forall i \in \mathcal{C})$
                            \State $\vect{x}^* = \argmax_{\vect{x} \in \mathcal{S}} f_{\mathcal{C}}(\vect{x})$
                        \EndWhile
                        \State \textbf{return} $\vect{x}^*, \mathcal{C}, f_{\mathcal{C}}$
                    \EndProcedure
                \end{algorithmic}
            \end{algorithm}
            \endgroup
        \end{column}
    \end{columns}
\end{frame}

\subsection{Experimental Study}
\begin{frame}[t, plain, squeeze]{}
    \begin{tikzpicture}[remember picture,overlay]
        \node[at=(current page.south),anchor=south] {
            \includegraphics[width=\paperwidth]{figures/goodwin-layout-wide.png}};
    \end{tikzpicture}
    \only<2-3>{
        \begin{textblock*}{0.59\textwidth}(5mm,25mm)
            \begin{alertblock}{Experimental Setup}
                \footnotesize
                \begin{itemize}
                    \item Location: Goodwin Hall $4^{th}$ floor
                    \item Number of subjects: 2
                    \item Prescriped track: 27 steps / pass / subject
                    \item Number of Passes: 6
                    \item Total Number of Steps: 162 / subject 
                    \item Sensors
                    \begin{itemize}
                        \footnotesize
                        \item PCB Piezotronics 352B
                        \item Number of sensors: 11
                        \item Sampling Frequency: 8192 $\si{\hertz}$
                        \item Dynamic Range 2-10,000 $\si{\hertz}$
                        \item Sensitivty 1,000 $\si{\milli\volt}$
                        \item Welded onto the flanges of I-beams
                    \end{itemize}
                \end{itemize}
            \end{alertblock}
        \end{textblock*} 
    }
    \only<3>{
        \begin{textblock*}{0.44\textwidth}(95mm,25mm)
            \begin{alertblock}{Calibration}
                \footnotesize
                \begin{itemize}
                    \item Spatial domain was discretized into {300~$\times$~900} grid cells
                    \item Training set: the first 27 steps of the first subject
                    \item Learned (calibrated) parameters
                    \begin{itemize}
                        \footnotesize
                        \item The propagation parameters $\vect{\beta}_i$
                        \item Signal noise envelope $\sigma_\zeta$
                    \end{itemize}
                \end{itemize}
            \end{alertblock}
        \end{textblock*}
    }
\end{frame}

\begin{frame}[t]{Procedure}
    \begin{figure}[T]
        \centering
        \includegraphics[width=0.7\textwidth]{figures/layout.png}
    \end{figure}
    Specifically, we employed all the possible sensor configurations with $m=\{2, \ldots, 11\}$ to localize the same step:
    \[
        \sum_{i=2}^{11} \binom{11}{i} = 2036
    \] 
    In other words, each step is reevaluated 2,036 times, yielding 329,832 data points for each occupant.
\end{frame}


\begin{frame}[t, squeeze]{Results -- \rnum{1}}
    \begin{columns}
        \begin{column}{0.33\textwidth}
            \begin{figure}[t]
                \centering
                \includegraphics[width=0.95\textwidth]{figures/results-1.png}
                \caption{An illustrative result of the $1^{st}$ occupant's data.}
            \end{figure}
        \end{column}
        \begin{column}{0.33\textwidth}
            \begingroup
                \footnotesize
                Localization outcomes for two distinct occupants using varying sensor counts ($m=2, 6, 11$).
                Square markers indicate sensor locations, circles denote non-Byzantine sensors, while green pluses and red crosses symbolize the ground truth and estimated locations, respectively.
                Errors for configurations (a) to (f) show progressive refinement with increased sensors, highlighting the algorithm's adaptability and precision.
            \endgroup
        \end{column}
        \begin{column}{0.33\textwidth}
            \begin{figure}[t]
                \centering
                \includegraphics[width=0.95\textwidth]{figures/results-2.png}
                \caption{An illustrative result of the $2^{nd}$ occupant's data.}
            \end{figure}
        \end{column}
    \end{columns}
\end{frame}

\begin{frame}[t]{Results -- \rnum{2}}
    \begin{columns}[T]
        \begin{column}{0.49\textwidth}
            \begin{figure}[T]
                \centering
                \includegraphics[width=0.99\textwidth]{figures/sensor-errors.png}
                \caption{\footnotesize Quartile analysis of sample localization errors against the number of sensors before the proposed \gls{bse} algorithm was employed. The plot showcases a consistent reduction in errors across all quartiles with an increasing number of sensors, highlighting improved consistency in both best- and worst-case scenarios.}
            \end{figure}
        \end{column}
        \begin{column}{0.49\textwidth}
            \begin{figure}[T]
                \centering
                \includegraphics[width=0.99\textwidth]{figures/sensor-entropies.png}
                \caption{\footnotesize Entropy-based precision of the localization system for varying sensor counts. Red and black lines differentiate data for the first and second occupants. The figure underscores reduced uncertainty with more sensors, highlighting enhanced precision across all quartiles.}
                \label{fig:entropy}
            \end{figure}
        \end{column}
    \end{columns}
\end{frame}
\begin{frame}[t]{Results -- \rnum{3}}
    \begin{figure}[htb!]
        \centering
        \includegraphics[scale=1]{figures/entropy-vs-error.png}
        \caption{This figure shows a Quantile-Quantile plot between the precision and accuracy metrics observed in the experimental data. The figure provides evidence for the correlation between precision and accuracy for varying numbers of sensors.}
        \label{fig:entropy-error}
    \end{figure}
\end{frame}
\begin{frame}[t]{Results -- \rnum{4}}
    \begin{figure}[htb!]
        \centering
        \includegraphics[scale=1]{figures/error-pdf.png}
        \caption{Empirical-\glspl{pdf} and \glspl{cdf} of normed localization errors derived from location estimates for both occupants. Solid lines represent the empirical-\glspl{pdf}, with blue and brown indicating the proposed and baseline techniques, respectively. Dashed lines depict the empirical-\glspl{cdf}. The plots demonstrate that the proposed technique generally results in lower localization errors compared to the baseline.}
        \label{fig:result-pdf}
    \end{figure}
\end{frame}

\begin{frame}[t, squeeze]{Results -- \rnum{5}}
    \begin{columns}[T]
        \begin{column}{0.48\textwidth}
            \begin{figure}[!htb]
                \centering
                \includegraphics[width=\textwidth]{figures/avg-distance-error_all.png}
                % \caption{\footnotesize Localization errors as a function of average sensor distance when all sensors were considered.}
                % \label{fig:hello-mirshekari-all}
            \end{figure}
        \end{column}
        \begin{column}{0.48\textwidth}
            \begin{figure}[!htb]
                \centering
                \includegraphics[width=\textwidth]{figures/avg-distance-error_bse.png}
                % \caption{\footnotesize Localization errors as a function of average sensor distance when a subset of the sensors were considered.}
                % \label{fig:hello-mirshekari-bse}
            \end{figure}
        \end{column}
    \end{columns}
    \begin{table}[htbp]
        \setlength{\tabcolsep}{6pt} % Default value: 6pt
        \setlength{\aboverulesep}{0pt}
        \setlength{\belowrulesep}{0pt}
        \renewcommand{\arraystretch}{1} % Default value: 1
        \footnotesize
        \begin{tabularx}{\linewidth}{@{} l C C C C}
            \toprule
            & \multicolumn{2}{c}{Work presented in~\cite{mirshekari2018occupant}} & \multicolumn{2}{c}{Proposed} \\
            \midrule
            & Without SE & With SE & Without SE & With SE \\
            Slope & 1.44 & 0.56 & \textbf{0.63} & \textbf{0.14} \\
            Intercept & \textbf{-2.3} & \textbf{-0.56} & -1.75  & 1.12 \\
            Correlation Coefficient & 0.82 & \textbf{0.24} & \textbf{0.37} & \textbf{0.24}\\
            \bottomrule
        \end{tabularx}
        \caption{A systemic comparison between the results of work presented in~\cite{mirshekari2018occupant} and the proposed localization technique. SE stands for Sensor Elimination.}
        \label{tab:mirshekari-occupant-1}
    \end{table}
\end{frame}


\begin{frame}{Summary of Contributions}
    \begin{enumerate}
        \normalsize
        \item Vibro-localization Technique with Comprehensive Uncertainty Quantification
        \item Information-Theoretic \gls{bse} Algorithm
        \item Empirical Validation
        \item Quantification of the Empirical Precision and Accuracy
    \end{enumerate}
\end{frame}
